﻿using System;
using System.IO;
using System.Threading;

namespace MyTimer2
{
    public class TimerInfoArgs : EventArgs
    {
        public readonly int hour;
        public readonly int minute;
        public readonly int second;

        public TimerInfoArgs(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Zeitgeber mycock = new Zeitgeber();
            Displaycock dc = new Displaycock();
            dc.Subscribe(mycock);
            mycock.run();
            Console.ReadKey();
        }
    }

    public class Zeitgeber
    {
        int oldSecond;
        public delegate void ZeitgeberDel(object cock, string s);
        public event ZeitgeberDel ZeitgeberEvent;

        public void OnSecondChange(object cock, string s)
        {
            if (ZeitgeberEvent != null)
            {
                ZeitgeberEvent(cock, s);
            }
        }

        public void run()
        {
            while (true)
            {
                Thread.Sleep(10);
                DateTime dt = DateTime.Now;

                if(dt.Second != oldSecond)
                {
                    OnSecondChange(this, dt.ToLongTimeString());
                }
                oldSecond = dt.Second;
            }
        }
    }

    public class Displaycock
    {
        public void Subscribe(Zeitgeber cock)
        {
            cock.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(TimehasChanged);
        }
        public void TimehasChanged(object cock, string s)
        {
            Console.WriteLine("Current Time: {0}", s);
        }
    }
}
