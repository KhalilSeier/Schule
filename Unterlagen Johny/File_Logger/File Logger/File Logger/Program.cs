﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace File_Logger
{
    class MyClass
    {
        public delegate void LogHandlerDel(string message);
        public void Process(LogHandlerDel logHandler)
        {
            if(logHandler != null)
            {
                logHandler("Process has been started ...");
                // ....
                logHandler("... Process has been stopped!");
            }
        }
    }

    class FileLogger
    {
        FileStream fileStream;
        StreamWriter streamWriter;

        public FileLogger(string filename)
        {
            fileStream = new FileStream(filename, FileMode.Create);
            streamWriter = new StreamWriter(fileStream);
        }

        public void Logger(string s)
        {
            DateTime dt = DateTime.Now;
            streamWriter.WriteLine(dt.ToString() + ":   " + s);
        }

        public void Logger2(string s)
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine(dt.ToString() + ":   " + s);
        }


        public void close()
        {
            streamWriter.Close();
            fileStream.Close();
        }

    }


    class Program
    {


        static void Main(string[] args)
        {



            FileLogger fl = new FileLogger("process.log");

            MyClass.LogHandlerDel handlerDel = null;
            handlerDel += fl.Logger;
            handlerDel += fl.Logger2;


            MyClass myClass = new MyClass();
            myClass.Process(handlerDel);


            handlerDel -= fl.Logger2;
            myClass.Process(handlerDel);

            fl.close();





            Console.ReadKey();
        }
    }
}
