﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.Diagnostics;

namespace FunctionVsDelegate
{

    public class ThirdPartyComponent
    {
        static private void SomePrivateMethod()
        {
            //do noting!!!
        }
    }

    public class MyWrapperFunct
    {
        private static MethodInfo myMethod = typeof(ThirdPartyComponent).GetMethod("SomePrivateMethod", BindingFlags.NonPublic | BindingFlags.Static);

        public void SomePublicMethod()
        {
            myMethod.Invoke(null, null);
        }
    }

    public class MyWrapperDelegate
    {
        private static MethodInfo myMethod = typeof(ThirdPartyComponent).GetMethod("SomePrivateMethod", BindingFlags.NonPublic | BindingFlags.Static);

        public delegate void MyDelegate();

        public static MyDelegate myDelegate = (MyDelegate)Delegate.CreateDelegate(typeof(MyDelegate), myMethod);
        public void SomePublicMethod()
        {
            myDelegate();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            //Function Call
            Stopwatch sw = Stopwatch.StartNew();
            MyWrapperFunct myWrapper = new MyWrapperFunct();

            const int iterations = 1000000;

            for(int i = 0; i < iterations; i++)
            {
                myWrapper.SomePublicMethod();
            }

            sw.Stop();
            Console.WriteLine("1000000 function calls: {0}ms ", sw.ElapsedMilliseconds);

            sw = Stopwatch.StartNew();
            MyWrapperDelegate myWrapperdel = new MyWrapperDelegate();

            for (int i = 0; i < iterations; i++)
            {
                myWrapperdel.SomePublicMethod();
            }
            sw.Stop();
            Console.WriteLine("1000000 delegate calls: {0}ms ", sw.ElapsedMilliseconds);

            Console.ReadKey();

        }
    }
}
