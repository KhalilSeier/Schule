﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;

namespace SemaphoreExample
{
    class Program
    {
        class mySemaphore
        {
            static Semaphore sem = new Semaphore(1, 1);
            private static int counter = 0;
            public void Increase()
            {
                while(true)
                {
                    sem.WaitOne();
                    Thread.Sleep(500);
                    counter++;
                    Console.WriteLine("Increase: {0}", counter);
                    sem.Release();
                }
            }

            public void Decrease()
            {
                while (true)
                {
                    sem.WaitOne();
                    Thread.Sleep(500);
                    counter--;
                    Console.WriteLine("Decrease: {0}", counter);
                    sem.Release();
                }
            }
        }
        static void Main(string[] args)
        {
            var mySemaphoreObject = new mySemaphore();

            Thread thread1, thread2;

            thread1 = new Thread(new ThreadStart(mySemaphoreObject.Increase));
            thread2 = new Thread(new ThreadStart(mySemaphoreObject.Decrease));

            thread1.Start();
            thread2.Start();

            Console.ReadKey();        
        }
    }
}
