﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace serializeRSS
{
    public class RssChannelItem
    {

        private string title;

        //[System.Xml.Serialization.XmlElementAttribute("title", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlElement("title", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private string link;

        //[System.Xml.Serialization.XmlElementAttribute("link", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlElement("link", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Link
        {
            get { return link; }
            set { link = value; }
        }

        private string description;

        //[System.Xml.Serialization.XmlElementAttribute("description", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlElement("description", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string pubDate;

        //[System.Xml.Serialization.XmlElementAttribute("pubDate", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlElement("pubDate", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string PubDate
        {
            get { return pubDate; }
            set { pubDate = value; }
        }
    }
}
