﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace serializeRSS
{
    class Program
    {
        static void Main(string[] args)
        {
            Rss myFeed = new Rss();

            myFeed.Channel.Titel = "Udo's RSS channel";
            myFeed.Channel.CopyRight = "Udo Payer";
            myFeed.Channel.Description = "Hier wird gezeigt, wie man eine RSS-Klasse serialisiert ...";
            myFeed.Channel.Language = "English";
            myFeed.Channel.Link = "http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=3739852&SiteID=1";
            myFeed.Channel.AddItem("Hier get es zu www.orf.at", 
                                   "http://www.orf.at", 
                                   "Das ist eine sehr informative Seite");
            myFeed.Channel.AddItem("Reading/Writing RSS using XMLSerializer.", 
                                   "http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=3739852&SiteID=1", 
                                   "Hier wird gezeigt, wie man ein RSS-Feed mittels XmlSerialisation erstellen kann");
            myFeed.Channel.AddItem("Kremlpartei verliert Zweidrittelmehrheit",
                                   "http://www.orf.at/stories/2092987/",
                                   "Die russische Regierungspartei Geeintes Russland hat die Parlamentswahl Prognosen zufolge mit 48,5 Prozent gewonnen");
            myFeed.Channel.AddItem("KGeheimdienst will aus für Netzfreiheit",
                                   "http://www.orf.at/stories/2092987/2092990/",
                                   "Mit einer beispiellosen Cyberattacke sind erstmals bei einer Parlamentswahl in Russland kremlkritische Internetseiten den ganzen Wahltag über blockiert worden");

            SerializerClass.Serialize(myFeed, @"rss.xml");

            System.Diagnostics.Process.Start(@"rss.xml"); 

            object returnedFeed = SerializerClass.Deserialize(myFeed, @"rss.xml");

            Rss savedFeed = returnedFeed as Rss;

            Console.WriteLine(
                string.Format("The newest newsitem is {0}",
                savedFeed[0].Title)
            );

            Console.ReadKey(true);
        }
    }
}
