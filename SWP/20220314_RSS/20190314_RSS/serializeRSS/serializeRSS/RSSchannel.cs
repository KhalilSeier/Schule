﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace serializeRSS
{
    public class RssChannel
    {

        private string title;

        [System.Xml.Serialization.XmlElement("title", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Titel
        {
            get { return title; }
            set { title = value; }
        }

        private string link;

        [System.Xml.Serialization.XmlElement("link", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Link
        {
            get { return link; }
            set { link = value; }
        }

        private string description;

        [System.Xml.Serialization.XmlElement("description", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string language;

        [System.Xml.Serialization.XmlElement("language", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string Language
        {
            get { return language; }
            set { language = value; }
        }

        private string copyRight;

        [System.Xml.Serialization.XmlElement("copyright", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string CopyRight
        {
            get { return copyRight; }
            set { copyRight = value; }
        }

        private string pubDate = System.DateTime.Now.ToLocalTime().ToString();

        [System.Xml.Serialization.XmlElement("pubDate", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public string PubDate
        {
            get { return pubDate; }
            set { pubDate = value; }
        }

        private List<RssChannelItem> items = new List<RssChannelItem>();
        
        [System.Xml.Serialization.XmlElement("item", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public RssChannelItem[] Items
        {
            get
            {
                return items.ToArray();
            }
            set
            {
                items.Clear();
                items.AddRange(value);
            }
        }

        public void AddItem(string title, string link, string description)
        {
            RssChannelItem theItem = new RssChannelItem();
            theItem.Title = title;
            theItem.Link = link;
            theItem.Description = description;
            theItem.PubDate = DateTime.Now.ToLocalTime().ToString();
            items.Insert(0, theItem);
            //New items go on top of the others... Not at the back.
        }

        public void DeleteItem(string title)
        {
            foreach (RssChannelItem item in this.items)
            {
                if (item.Title == title) this.items.Remove(item);
                return;
            }
        }

        public void ClearItems()
        {
            this.items.Clear();
        }

        public RssChannelItem FindItem(string title)
        {
            foreach (RssChannelItem item in this.items)
            {
                if (item.Title == title) return item;
            }
            return null;
        }
    }
}
