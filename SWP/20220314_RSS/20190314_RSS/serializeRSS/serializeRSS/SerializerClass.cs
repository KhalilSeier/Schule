﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace serializeRSS
{
    public abstract class SerializerClass
    {
        public static void Serialize(object theObject, string thePath) 
        {
            XmlSerializer theSerializer = new XmlSerializer(theObject.GetType());

            using (XmlWriter theWriter = XmlWriter.Create(thePath))
            {
                theSerializer.Serialize(theWriter, theObject);
            }
        }

        public static object Deserialize(object theObject, string thePath) 
        {
            XmlSerializer theSerializer = new XmlSerializer(theObject.GetType());

            using (XmlReader theReader = XmlReader.Create(thePath))
            {
                return theSerializer.Deserialize(theReader);
            }
        }
    }
}
