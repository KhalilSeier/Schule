﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace serializeRSS
{
    [System.Xml.Serialization.XmlRoot("rss")]

    public class Rss
    {
        private decimal version = 2.0m;

        [System.Xml.Serialization.XmlAttribute("version", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public decimal Version
        {
            get { return version; }
            set { version = value; }
        }

        private RssChannel channel = new RssChannel();

        [System.Xml.Serialization.XmlElement("channel", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]

        public RssChannel Channel
        {
            get { return channel; }
            set { channel = value; }
        }

        [System.Xml.Serialization.XmlIgnore()]

        public RssChannelItem this[string title]
        {
            get { return this.Channel.FindItem(title); }
        }

        [System.Xml.Serialization.XmlIgnore()]

        public RssChannelItem this[int index]
        {
            get
            {
                if (this.channel.Items.GetLength(0) == 0)
                    return null;
                return this.channel.Items[0];
            }
        }
    }
}
