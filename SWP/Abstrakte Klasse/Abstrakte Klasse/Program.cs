﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Abstrakte_Klasse
{
    abstract class Tiere
    {
        protected double initialGewicht;
        private double aktuellesGewicht;
        private bool isAlive;
        public double AktuellesGewicht
        {
            get { return aktuellesGewicht; }
            set { aktuellesGewicht = value; }
        }
        public double InitialGewicht
        {
            get { return initialGewicht; }
            set { initialGewicht = value; }
        }
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }


        public Tiere(double initialGewicht2)
        {
            aktuellesGewicht = initialGewicht2;
            initialGewicht = initialGewicht2;
            isAlive = true;
        }

        protected abstract bool canEat(Tiere tier);

        public void wirdGetoetet()
        {
            isAlive = false;
        }

        public void eat(Tiere tier/*Eichhörnchen*/)
        {
            bool e;
            e = canEat(tier);
            //if(canEat(tier))
            if(e)
            {
                /*T-Rex*/this.aktuellesGewicht += /*Eichhörnchen*/tier.AktuellesGewicht;
                tier.wirdGetoetet();
                Console.Write("gefressen!");
            }
            else
            {
                Console.Write("nicht gefressen!");
            }
        }

        public void Verdauen()
        {
            if(aktuellesGewicht>initialGewicht)
            {
                aktuellesGewicht = initialGewicht +(aktuellesGewicht-initialGewicht)*0.95;
            }
        }
    }

    class Eichhoernchen : Tiere
    {
        public Eichhoernchen(double initialGewicht2) : base(initialGewicht2)
        {
        }

        protected override bool canEat(Tiere tier)
        {
            return false;
        }
    }
    class Baummarder : Tiere
    {
        public Baummarder(double initialGewicht2) : base(initialGewicht2)
        {

        }

        protected override bool canEat(Tiere tier)
        {
            if (tier.ToString() == "Abstrakte_Klasse.Eichhoernchen" && tier.IsAlive == true && IsAlive == true)
            { return true; }
            else { return false; }
        }
    }
    class Uhu : Tiere
    {
        public Uhu(double initialGewicht2) : base(initialGewicht2)
        {
        }

        protected override bool canEat(Tiere tier)
        {
            if(tier.ToString() != "Abstrakte_Klasse.Uhu" && tier.IsAlive == true && IsAlive == true)
            {
                return true;
            }
            else { return false; }
        }

        public void fliegen()
        {
            if(AktuellesGewicht-10<initialGewicht)
            {
                AktuellesGewicht=initialGewicht;
            }
            else
            {
                AktuellesGewicht -= 10;
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Tiere tier1;
            Tiere tier2;

            Random rnd = new Random();
            int r = 1;

            while (true)
            {
                Console.WriteLine();
                r = rnd.Next(1, 4);
                tier1 = RndTier(r);
                r = rnd.Next(1, 4);
                tier2 = RndTier(r);

                Console.WriteLine("{0} trifft auf {1}. {0} versucht {1} zu fressen. {1} wird ",tier1.ToString().Substring(17), tier2.ToString().Substring(17));
                tier1.eat(tier2);
                Console.WriteLine();
                Thread.Sleep(1000);
            }
        }
        static Tiere RndTier(int r)
        {
            Tiere t1;
            if (r == 1)
            {
                t1 = new Eichhoernchen(50);
            }
            else if (r == 2)
            {
                t1 = new Baummarder(90);
            }
            else
            {
                t1 = new Uhu(150);
            }
            return t1;
        }
    }
}
