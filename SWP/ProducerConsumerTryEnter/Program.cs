﻿using System;
using System.Threading;

namespace ProducerConsumerTryEnter
{
    class MyClass
    {
        public int intVAr = 0;
        public bool available = false;
        Random rnd = new Random();


        public void Producer()
        {
            bool getIn = false;
            while(true)
            {
                Monitor.TryEnter(this,ref getIn);
                if(getIn)
                {
                    if(!available)
                    {
                        intVAr = rnd.Next(0, 100);
                        Console.WriteLine("Producer: {0}", intVAr);
                        available = true;
                    }
                    getIn = false;
                    Monitor.Exit(this);
                }

            }
        }

        public void Consumer()
        {
            bool getIn = false;
            while (true)
            {
                Monitor.TryEnter(this, ref getIn);
                if (getIn)
                {
                    if (available)
                    {
                        Console.WriteLine("Consumer: {0}", intVAr);
                        available = false;
                    }
                    getIn = false;
                    Monitor.Exit(this);
                }

            }
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass myObj = new MyClass();
            Thread thread1, thread2;

            thread1 = new Thread(new ThreadStart(myObj.Producer));
            thread2 = new Thread(new ThreadStart(myObj.Consumer));

            thread1.Start();
            thread2.Start();
        }
    }
}
