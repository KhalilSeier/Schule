﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeDesirialise
{
    class Program
    {
        static void Main(string[] args)
        {
            myClass person = new myClass("Udo Payer", "Cobacabana 4", "A-8401", new DateTime(1965,7,15));
            Serialize mySerializer = new Serialize();

            mySerializer.Serializ(person, "output.xml");
            Console.WriteLine("output.xml wurde erzeugt ...");

            Console.ReadKey();

            myClass newPerson = new myClass();
            newPerson = (myClass)mySerializer.Deserialize(newPerson,"output.xml");

        }
    }
}
