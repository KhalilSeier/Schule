﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeDesirialise
{
    public class myClass
    {
        private string name;
        private string street;
        private string zip;
        private DateTime b_day;

        public myClass()
        {

        }
        
        public myClass(string name, string street, string zip, DateTime b_day)
        {
            this.name = name;
            this.street = street;
            this.zip = zip;
            this.b_day = b_day;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Zip
        {
            get { return zip; }
            set { zip = value; }
        }
        public DateTime B_day
        {
            get { return b_day; }
            set { b_day = value; }
        }
        public string Street
        {
            get { return street; }
            set { street = value; }
        }
    }
}
