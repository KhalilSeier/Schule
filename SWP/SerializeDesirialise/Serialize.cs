﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;

namespace SerializeDesirialise
{
    public class Serialize
    {
        public void Serializ(object theobject,string theFilename)
        {
            XmlSerializer theSerializer = new XmlSerializer(theobject.GetType());
            using (XmlWriter theWriter = XmlWriter.Create(theFilename))
            {
                theSerializer.Serialize(theWriter, theobject);
            }
        }

        public void Deserialize(object theObject, string theFilename)
        {
            XmlSerializer theSerializer = new XmlSerializer(theObject.GetType());
            using (XmlReader theReader = XmlReader.Create(theFilename))
            {
                theSerializer.Deserialize(theReader);
            }
        }
    }
}
