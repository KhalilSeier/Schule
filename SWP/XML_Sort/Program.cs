﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace XML_Sort
{
    class Program
    {

        public static string FILE_NAME = "food_menu.xml";
        public static string FILE_NAME_PRICE = Environment.CurrentDirectory + @"\food_menu_sorted_prices.xml";

        static void Main(string[] args)
        {
            SortPrice();
            Console.ReadKey();
        }
        public static void SortPrice()
        {
            XPathDocument doc = new XPathDocument(FILE_NAME);
            XPathNavigator nav = doc.CreateNavigator();
            XPathExpression expr = nav.Compile("breakfast_menu/food/price");
            XPathNodeIterator iterator = nav.Select(expr);

            ArrayList prices = new ArrayList();
            
            try
            {
                while(iterator.MoveNext())
                {
                    prices.Add(iterator.Current.Value);
                }
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
            prices.Sort();

            XmlTextReader reader = new XmlTextReader(FILE_NAME);
            XmlDocument newDoc = new XmlDocument();

            newDoc.Load(reader);
            reader.Close();

            XmlElement root = newDoc.DocumentElement;

            foreach(string price in prices)
            {
                XmlNode foodNode = root.SelectSingleNode(string.Format("/breakfast_menu/food[price = '{0}' ", price));
                XmlNode parent = foodNode.ParentNode;
                parent.RemoveChild(foodNode);
                parent.InsertAfter(foodNode, parent.LastChild);
            }
            newDoc.Save(FILE_NAME_PRICE);
        }
    }
}
