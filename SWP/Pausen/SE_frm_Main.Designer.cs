﻿
namespace Pausen
{
    partial class SE_frm_Main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_khalilSeier = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button18 = new System.Windows.Forms.Button();
            this.tmr_seconds = new System.Windows.Forms.Timer(this.components);
            this.tmr_uk = new System.Windows.Forms.Timer(this.components);
            this.labl_khalilSeier = new System.Windows.Forms.Label();
            this.lbl_michaelWilhelm = new System.Windows.Forms.Label();
            this.lbl_berbotovicArdit = new System.Windows.Forms.Label();
            this.ocherbauerAnna = new System.Windows.Forms.Label();
            this.hoferMariella = new System.Windows.Forms.Label();
            this.lbl_pokornyLena = new System.Windows.Forms.Label();
            this.lbl_hofingerMoritz = new System.Windows.Forms.Label();
            this.lbl_aaronBengi = new System.Windows.Forms.Label();
            this.lbl_pflanzlLukas = new System.Windows.Forms.Label();
            this.lbl_kandhoferNadine = new System.Windows.Forms.Label();
            this.lbl_oneRahela = new System.Windows.Forms.Label();
            this.lbl_zornSophie = new System.Windows.Forms.Label();
            this.lbl_harbEva = new System.Windows.Forms.Label();
            this.lbl_baierCheyenne = new System.Windows.Forms.Label();
            this.lbl_GauglCornellia = new System.Windows.Forms.Label();
            this.lbl_knollCelina = new System.Windows.Forms.Label();
            this.lbl_mayerBelinda = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_khalilSeier
            // 
            this.btn_khalilSeier.Location = new System.Drawing.Point(26, 77);
            this.btn_khalilSeier.Name = "btn_khalilSeier";
            this.btn_khalilSeier.Size = new System.Drawing.Size(146, 36);
            this.btn_khalilSeier.TabIndex = 0;
            this.btn_khalilSeier.Text = "Pause Starten";
            this.btn_khalilSeier.UseVisualStyleBackColor = true;
            this.btn_khalilSeier.Click += new System.EventHandler(this.btn_khalilSeier_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(178, 77);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 36);
            this.button2.TabIndex = 1;
            this.button2.Text = "Pause Starten";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(483, 77);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(146, 36);
            this.button3.TabIndex = 3;
            this.button3.Text = "Pause Starten";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(331, 77);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(146, 36);
            this.button4.TabIndex = 2;
            this.button4.Text = "Pause Starten";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(483, 187);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(146, 36);
            this.button5.TabIndex = 7;
            this.button5.Text = "Pause Starten";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(331, 187);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(146, 36);
            this.button6.TabIndex = 6;
            this.button6.Text = "Pause Starten";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(178, 187);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(146, 36);
            this.button7.TabIndex = 5;
            this.button7.Text = "Pause Starten";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(26, 187);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(146, 36);
            this.button8.TabIndex = 4;
            this.button8.Text = "Pause Starten";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(635, 187);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(146, 36);
            this.button9.TabIndex = 9;
            this.button9.Text = "Pause Starten";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(635, 77);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(146, 36);
            this.button10.TabIndex = 8;
            this.button10.Text = "Pause Starten";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(635, 300);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(146, 36);
            this.button11.TabIndex = 14;
            this.button11.Text = "Pause Starten";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(483, 300);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(146, 36);
            this.button12.TabIndex = 13;
            this.button12.Text = "Pause Starten";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(331, 300);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(146, 36);
            this.button13.TabIndex = 12;
            this.button13.Text = "Pause Starten";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(178, 300);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(146, 36);
            this.button14.TabIndex = 11;
            this.button14.Text = "Pause Starten";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(26, 300);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(146, 36);
            this.button15.TabIndex = 10;
            this.button15.Text = "Pause Starten";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(411, 418);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(146, 36);
            this.button16.TabIndex = 16;
            this.button16.Text = "Pause Starten";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(259, 418);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(146, 36);
            this.button17.TabIndex = 15;
            this.button17.Text = "Pause Starten";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 10);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // button18
            // 
            this.button18.Enabled = false;
            this.button18.Location = new System.Drawing.Point(12, 0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(74, 25);
            this.button18.TabIndex = 18;
            this.button18.Text = "weiter";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // labl_khalilSeier
            // 
            this.labl_khalilSeier.AutoSize = true;
            this.labl_khalilSeier.Location = new System.Drawing.Point(52, 45);
            this.labl_khalilSeier.Name = "labl_khalilSeier";
            this.labl_khalilSeier.Size = new System.Drawing.Size(79, 17);
            this.labl_khalilSeier.TabIndex = 19;
            this.labl_khalilSeier.Text = "Khalil Seier";
            // 
            // lbl_michaelWilhelm
            // 
            this.lbl_michaelWilhelm.AutoSize = true;
            this.lbl_michaelWilhelm.Location = new System.Drawing.Point(194, 45);
            this.lbl_michaelWilhelm.Name = "lbl_michaelWilhelm";
            this.lbl_michaelWilhelm.Size = new System.Drawing.Size(109, 17);
            this.lbl_michaelWilhelm.TabIndex = 20;
            this.lbl_michaelWilhelm.Text = "Michael Wilhelm";
            // 
            // lbl_berbotovicArdit
            // 
            this.lbl_berbotovicArdit.AutoSize = true;
            this.lbl_berbotovicArdit.Location = new System.Drawing.Point(353, 45);
            this.lbl_berbotovicArdit.Name = "lbl_berbotovicArdit";
            this.lbl_berbotovicArdit.Size = new System.Drawing.Size(108, 17);
            this.lbl_berbotovicArdit.TabIndex = 21;
            this.lbl_berbotovicArdit.Text = "Berbatovci Ardit";
            // 
            // ocherbauerAnna
            // 
            this.ocherbauerAnna.AutoSize = true;
            this.ocherbauerAnna.Location = new System.Drawing.Point(502, 45);
            this.ocherbauerAnna.Name = "ocherbauerAnna";
            this.ocherbauerAnna.Size = new System.Drawing.Size(121, 17);
            this.ocherbauerAnna.TabIndex = 22;
            this.ocherbauerAnna.Text = "Ocherbauer Anna";
            // 
            // hoferMariella
            // 
            this.hoferMariella.AutoSize = true;
            this.hoferMariella.Location = new System.Drawing.Point(657, 45);
            this.hoferMariella.Name = "hoferMariella";
            this.hoferMariella.Size = new System.Drawing.Size(96, 17);
            this.hoferMariella.TabIndex = 23;
            this.hoferMariella.Text = "Hofer Mariella";
            // 
            // lbl_pokornyLena
            // 
            this.lbl_pokornyLena.AutoSize = true;
            this.lbl_pokornyLena.Location = new System.Drawing.Point(194, 158);
            this.lbl_pokornyLena.Name = "lbl_pokornyLena";
            this.lbl_pokornyLena.Size = new System.Drawing.Size(96, 17);
            this.lbl_pokornyLena.TabIndex = 25;
            this.lbl_pokornyLena.Text = "Pokorny Lena";
            // 
            // lbl_hofingerMoritz
            // 
            this.lbl_hofingerMoritz.AutoSize = true;
            this.lbl_hofingerMoritz.Location = new System.Drawing.Point(42, 158);
            this.lbl_hofingerMoritz.Name = "lbl_hofingerMoritz";
            this.lbl_hofingerMoritz.Size = new System.Drawing.Size(104, 17);
            this.lbl_hofingerMoritz.TabIndex = 24;
            this.lbl_hofingerMoritz.Text = "Hofinger Moritz";
            // 
            // lbl_aaronBengi
            // 
            this.lbl_aaronBengi.AutoSize = true;
            this.lbl_aaronBengi.Location = new System.Drawing.Point(502, 158);
            this.lbl_aaronBengi.Name = "lbl_aaronBengi";
            this.lbl_aaronBengi.Size = new System.Drawing.Size(86, 17);
            this.lbl_aaronBengi.TabIndex = 27;
            this.lbl_aaronBengi.Text = "Aaron Bengi";
            // 
            // lbl_pflanzlLukas
            // 
            this.lbl_pflanzlLukas.AutoSize = true;
            this.lbl_pflanzlLukas.Location = new System.Drawing.Point(353, 158);
            this.lbl_pflanzlLukas.Name = "lbl_pflanzlLukas";
            this.lbl_pflanzlLukas.Size = new System.Drawing.Size(92, 17);
            this.lbl_pflanzlLukas.TabIndex = 26;
            this.lbl_pflanzlLukas.Text = "Pflanzl Lukas";
            // 
            // lbl_kandhoferNadine
            // 
            this.lbl_kandhoferNadine.AutoSize = true;
            this.lbl_kandhoferNadine.Location = new System.Drawing.Point(645, 158);
            this.lbl_kandhoferNadine.Name = "lbl_kandhoferNadine";
            this.lbl_kandhoferNadine.Size = new System.Drawing.Size(126, 17);
            this.lbl_kandhoferNadine.TabIndex = 28;
            this.lbl_kandhoferNadine.Text = "Kandlhofer Nadine";
            // 
            // lbl_oneRahela
            // 
            this.lbl_oneRahela.AutoSize = true;
            this.lbl_oneRahela.Location = new System.Drawing.Point(657, 270);
            this.lbl_oneRahela.Name = "lbl_oneRahela";
            this.lbl_oneRahela.Size = new System.Drawing.Size(92, 17);
            this.lbl_oneRahela.TabIndex = 33;
            this.lbl_oneRahela.Text = "Onea Rahela";
            // 
            // lbl_zornSophie
            // 
            this.lbl_zornSophie.AutoSize = true;
            this.lbl_zornSophie.Location = new System.Drawing.Point(505, 270);
            this.lbl_zornSophie.Name = "lbl_zornSophie";
            this.lbl_zornSophie.Size = new System.Drawing.Size(86, 17);
            this.lbl_zornSophie.TabIndex = 32;
            this.lbl_zornSophie.Text = "Zorn Sophie";
            // 
            // lbl_harbEva
            // 
            this.lbl_harbEva.AutoSize = true;
            this.lbl_harbEva.Location = new System.Drawing.Point(368, 270);
            this.lbl_harbEva.Name = "lbl_harbEva";
            this.lbl_harbEva.Size = new System.Drawing.Size(67, 17);
            this.lbl_harbEva.TabIndex = 31;
            this.lbl_harbEva.Text = "Harb Eva";
            // 
            // lbl_baierCheyenne
            // 
            this.lbl_baierCheyenne.AutoSize = true;
            this.lbl_baierCheyenne.Location = new System.Drawing.Point(197, 270);
            this.lbl_baierCheyenne.Name = "lbl_baierCheyenne";
            this.lbl_baierCheyenne.Size = new System.Drawing.Size(109, 17);
            this.lbl_baierCheyenne.TabIndex = 30;
            this.lbl_baierCheyenne.Text = "Baier Cheyenne";
            // 
            // lbl_GauglCornellia
            // 
            this.lbl_GauglCornellia.AutoSize = true;
            this.lbl_GauglCornellia.Location = new System.Drawing.Point(45, 270);
            this.lbl_GauglCornellia.Name = "lbl_GauglCornellia";
            this.lbl_GauglCornellia.Size = new System.Drawing.Size(102, 17);
            this.lbl_GauglCornellia.TabIndex = 29;
            this.lbl_GauglCornellia.Text = "Gaugl Cornelia";
            // 
            // lbl_knollCelina
            // 
            this.lbl_knollCelina.AutoSize = true;
            this.lbl_knollCelina.Location = new System.Drawing.Point(431, 389);
            this.lbl_knollCelina.Name = "lbl_knollCelina";
            this.lbl_knollCelina.Size = new System.Drawing.Size(82, 17);
            this.lbl_knollCelina.TabIndex = 35;
            this.lbl_knollCelina.Text = "Knoll Celina";
            // 
            // lbl_mayerBelinda
            // 
            this.lbl_mayerBelinda.AutoSize = true;
            this.lbl_mayerBelinda.Location = new System.Drawing.Point(279, 389);
            this.lbl_mayerBelinda.Name = "lbl_mayerBelinda";
            this.lbl_mayerBelinda.Size = new System.Drawing.Size(98, 17);
            this.lbl_mayerBelinda.TabIndex = 34;
            this.lbl_mayerBelinda.Text = "Mayer Belinda";
            // 
            // SE_frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 503);
            this.Controls.Add(this.lbl_knollCelina);
            this.Controls.Add(this.lbl_mayerBelinda);
            this.Controls.Add(this.lbl_oneRahela);
            this.Controls.Add(this.lbl_zornSophie);
            this.Controls.Add(this.lbl_harbEva);
            this.Controls.Add(this.lbl_baierCheyenne);
            this.Controls.Add(this.lbl_GauglCornellia);
            this.Controls.Add(this.lbl_kandhoferNadine);
            this.Controls.Add(this.lbl_aaronBengi);
            this.Controls.Add(this.lbl_pflanzlLukas);
            this.Controls.Add(this.lbl_pokornyLena);
            this.Controls.Add(this.lbl_hofingerMoritz);
            this.Controls.Add(this.hoferMariella);
            this.Controls.Add(this.ocherbauerAnna);
            this.Controls.Add(this.lbl_berbotovicArdit);
            this.Controls.Add(this.lbl_michaelWilhelm);
            this.Controls.Add(this.labl_khalilSeier);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_khalilSeier);
            this.Name = "SE_frm_Main";
            this.Load += new System.EventHandler(this.SE_frm_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_khalilSeier;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Timer tmr_seconds;
        private System.Windows.Forms.Timer tmr_uk;
        private System.Windows.Forms.Label labl_khalilSeier;
        private System.Windows.Forms.Label lbl_michaelWilhelm;
        private System.Windows.Forms.Label lbl_berbotovicArdit;
        private System.Windows.Forms.Label ocherbauerAnna;
        private System.Windows.Forms.Label hoferMariella;
        private System.Windows.Forms.Label lbl_pokornyLena;
        private System.Windows.Forms.Label lbl_hofingerMoritz;
        private System.Windows.Forms.Label lbl_aaronBengi;
        private System.Windows.Forms.Label lbl_pflanzlLukas;
        private System.Windows.Forms.Label lbl_kandhoferNadine;
        private System.Windows.Forms.Label lbl_oneRahela;
        private System.Windows.Forms.Label lbl_zornSophie;
        private System.Windows.Forms.Label lbl_harbEva;
        private System.Windows.Forms.Label lbl_baierCheyenne;
        private System.Windows.Forms.Label lbl_GauglCornellia;
        private System.Windows.Forms.Label lbl_knollCelina;
        private System.Windows.Forms.Label lbl_mayerBelinda;
    }
}

