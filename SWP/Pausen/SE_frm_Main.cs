﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pausen
{
    public partial class SE_frm_Main : Form
    {
        public bool khalilPause = false;
        public bool michaelPause = false;
        public bool arditPause = false;
        public bool annaPause = false;
        public bool mariellaPause = false;
        public bool moritzPause = false;
        public bool lenaPause = false;
        public bool lukasPause = false;
        public bool bengiPause = false;
        public bool nadinePause = false;
        public bool corneliaPause = false;
        public bool cheyennePause = false;
        public bool evaPause = false;
        public bool sophiePause = false;
        public bool rahelaPause = false;
        public bool belindaPause = false;
        public bool celindaPause = false;

        WriteAndReadTextfile writeAndRead = new WriteAndReadTextfile();
        public SE_frm_Main()
        {
            InitializeComponent();
        }

        private void SE_frm_Main_Load(object sender, EventArgs e)
        {
            MessageBox.Show(DateTime.Now.ToString("HH:mm:ss"));
        }

        private void btn_khalilSeier_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            NamenUebergeben(labl_khalilSeier.Text);
        }
        private void NamenUebergeben(string name)
        {
            writeAndRead.WriteInTextfile(name);
        }
    }
}
