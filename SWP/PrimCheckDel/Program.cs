﻿using System;

using System.Threading;

namespace PrimCheckDel
{
    class Program
    {
        static void Main(string[] args)
        {
            WritePrim writePrim = new WritePrim();
            
        }
        public void PrimePrint(int prime)
        {
            Console.WriteLine(prime.ToString());
        }

    }
    public class WritePrim
    {
        public delegate string CheckPrimDelegate(int number);
        public event CheckPrimDelegate CheckPrimEvent;

        public void Process()
        {
            for (int i = 0; i < 100; i++)
            {
                if (CheckPrim(i))
                {
                    OnLog(i);
                }
                Thread.Sleep(200);
            }
        }
        static bool CheckPrim(int number)
        {
            if (number <= 1) return false;
            for (int i = 2; i < number; i++)
            {
                if (number % i == 0) return false;
            }
            return true;
        }

        static void OnLog(int prime)
        {
            //if (CheckPrimEvent != null)
            //{
            //    CheckPrimEvent(prime);
            //}
        }
        

    }
}
