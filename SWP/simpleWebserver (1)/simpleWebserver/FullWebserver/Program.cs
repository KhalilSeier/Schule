﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace FullWebserver
{
    class cls_Server
    {
        private Socket socket;
        public cls_Server(Socket socket)
        {
            this.socket = socket;
        }
        public void Run()
        {
            while (true)
            {
                Thread.Sleep(500);
                string request = GetContent();

                //if(request.Length != 0)
                //    Console.WriteLine("> " + request + Environment.NewLine);

                string[] lines = request.Split(Environment.NewLine.ToCharArray());
                string[] words = lines[0].Split(' ');
                if (words[0] == "GET")
                {
                    int position = words[1].LastIndexOf(@"/");
                    if (words[1] != @"/" && words[1].Length > position)
                    {
                        string filename = words[1].Substring(position + 1);
                        Console.WriteLine("> " + filename);
                        if(File.Exists(Environment.CurrentDirectory + @"\files\" + filename))
                        {
                            SendFile(Environment.CurrentDirectory + @"\files\" + filename);
                        }
                    } 
                }
            }
        }
        public void SendFile(string filename)
        {
            FileStream stream = new FileStream(filename, FileMode.Open);
            SendText("HTTP/1.1 200 OK");
            SendText("Server: myServer 1.0");
            SendText("Content-Type: text/html");
            SendText("Content-Length: " + stream.Length);
            SendText("Connection: close");
            SendText("");


            long read = 0;
            while(read < stream.Length)
            {
                int buffersize = 1024;
                if (stream.Length - read > 1024)
                {
                    buffersize = 1024;
                }
                else 
                {
                    buffersize = (int)(stream.Length - read);
                    read = stream.Length;
                }

                byte[] toSend = new byte[buffersize];
                stream.Read(toSend, 0, buffersize);
                socket.Send(toSend);
                Thread.Sleep(10);
            }
            stream.Close();
        }
        void SendText(string serverResponse)
        {
            byte[] sendByte = Encoding.ASCII.GetBytes(serverResponse + Environment.NewLine);
            socket.Send(sendByte);
            Console.WriteLine("> " + serverResponse);
        }
        public string GetContent()
        {
            string s = String.Empty;
            while(socket.Available > 0)
            {
                int bytes = socket.Available;
                byte[] buffer = new byte[bytes];
                socket.Receive(buffer, bytes, SocketFlags.None);
                s += Encoding.ASCII.GetString(buffer);
            }
            return s;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string ipAddress = "127.0.0.1";
            IPAddress remoteAddress = IPAddress.Parse(ipAddress);
            TcpListener server = new TcpListener(remoteAddress, 80);
            server.Start();
            while(true)
            {
                Socket socket = server.AcceptSocket();
                cls_Server myServer = new cls_Server(socket);
                Thread clientThread = new Thread(myServer.Run);
                clientThread.Start();
            }
        }
    }
}
