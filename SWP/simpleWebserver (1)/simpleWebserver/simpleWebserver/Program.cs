﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace simpleWebserver
{
    class Program
    {
        static string ipAddress = "127.0.0.1";
        //static string ipAddress = "10.20.0.34";
        static IPAddress remoteAddress = IPAddress.Parse(ipAddress);

        static int requestCount = 0;
        static TcpListener serverSocket = new TcpListener(remoteAddress, 8080);
        static Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        static void run()
        {
            while (true)
            {
                Thread.Sleep(500);
                try
                {
                    if (clientSocket.Available != 0)
                    {
                        Console.WriteLine("Available = " + clientSocket.Available.ToString());
                        int bytes = clientSocket.Available;
                        byte[] buffer = new byte[bytes];
                        clientSocket.Receive(buffer, bytes, SocketFlags.None);
                        string dataFromClient = Encoding.ASCII.GetString(buffer);
                        Console.WriteLine("> Daten vom Client: " +
                            Environment.NewLine + dataFromClient);

                        try
                        {
                            string HtmlText = "<HTML><HEAD><TITLE>Das ist der Titel</TITLE></HEAD><BODY>teststring</BODY></HTML>";
                            int httpLength = HtmlText.Length;
                            SendText("HTTP/1.1 200 OK");
                            SendText("Server: myServer 1.0");
                            SendText("Content-Type: text/html");
                            SendText("Content-Length: " + httpLength);
                            SendText("Connection: close");
                            SendText("");
                            SendText(HtmlText);

                            clientSocket = serverSocket.AcceptSocket();
                        }
                        catch { }
                    }
                    else { }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                }  
            }
        }

        static void SendText(string serverResponse)
        {
            byte[] sendByte = Encoding.ASCII.GetBytes(serverResponse + Environment.NewLine);
            clientSocket.Send(sendByte);
            Console.WriteLine("> " + serverResponse);
        }

        static void Main(string[] args)
        {
            serverSocket.Start();
            Console.WriteLine("> Server wurde gestartet ...");
            clientSocket = serverSocket.AcceptSocket();
            Console.WriteLine("> Verbindung wurde angenommen...");
            Thread myThread = new Thread(run);
            myThread.Start();
            myThread.Join();
            Console.WriteLine("> Server wurde gestoppt");
            Console.ReadKey();
        }
    }
}
