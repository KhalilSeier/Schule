﻿using System;
using System.Reflection;

namespace Delegates
{
    class Program
    {
        public delegate void SimpleDelegate();
            


        static void Main(string[] args)
        {

            // 1. Möglichkeit
            //ThirdPartyComponent thirdPartyComponent = new ThirdPartyComponent();
            //thirdPartyComponent.MyFunction();

            //2. Möglichkeit
            //ThirdPartyComponent.MyFunction();

            //3 Möglishkeit
            //method = typeof(ThirdPartyComponent).GetMethod("MyFunction",BindingFlags.Static | BindingFlags.Public);
            //method.Invoke(null,null);

            //4. Möglichkeit
            //SimpleDelegate mySimpleDelegates = new SimpleDelegate(ThirdPartyComponent.MyFunction);
            //mySimpleDelegates.Invoke();

            object[] a = new object[2] {1,"a"};
            a[0] = 1;
            a[1] = "a";
            method = typeof(ThirdPartyComponent).GetMethod("MyFunction2", BindingFlags.Static | BindingFlags.Public);
            method.Invoke(null, a);
        }
        public class ThirdPartyComponent
        {
            public static void MyFunction()
            {
                Console.WriteLine("... bin in der Methode MyFunction()");
            }
            public static void MyFunction2(int a, string b)
            {
                Console.WriteLine("... bin in der Methode MyFunction()");
            }
        }
        private static MethodInfo method;
    }
}
