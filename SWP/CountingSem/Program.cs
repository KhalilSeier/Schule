﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Numerics;

namespace CountingSem
{
    class Program
    {
        class mySemaphore
        {
            public int threadzaehler = 0;
            public mySemaphore(int zaehler)
            {
                threadzaehler = zaehler+1;
            }
            public static Semaphore sem = new Semaphore(2, 2);

            public void ProbeThread()
            {
                while (true)
                {
                    sem.WaitOne();
                    Console.WriteLine("Thread {0} is in", threadzaehler);
                    //Thread.Sleep(500);
                    Console.WriteLine("Thread {0} is out", threadzaehler);
                    Console.WriteLine("-----------------------------------------------------------");
                    sem.Release();
                }
            }
        }

        static void Main(string[] args)
        {
            List<Thread> ListThreads = new List<Thread>(); 
            List<mySemaphore> ListmySemaphores = new List<mySemaphore>();
            for(int i =0; i<100; i++)
            {
                ListmySemaphores.Add(new mySemaphore(i));
                ListThreads.Add(new Thread(new ThreadStart(ListmySemaphores[i].ProbeThread)));
                ListThreads[i].Start();
            }
        }
    }
}
