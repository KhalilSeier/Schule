﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace TCPserver
{
    class Program
    {
        static string ipAddress = "127.0.0.1";
        static IPAddress remoteAddress = IPAddress.Parse(ipAddress);

        static TcpListener serverSocket = new TcpListener(remoteAddress, 8080);
        static Socket clientSockel = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

        static void run()
        { 
            while(true)
            {
                Thread.Sleep(500);
                try
                {
                    if(clientSockel.Available != 0)
                    {
                        int bytes = clientSockel.Available;
                        byte[] buffer = new byte[bytes];
                        clientSockel.Receive(buffer, bytes, SocketFlags.None);
                        string datafromClient = Encoding.ASCII.GetString(buffer);
                        Console.WriteLine("> " + datafromClient);

                        sendText("< " +datafromClient);
                    }
                }
                catch(Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                }
            }
        }
        static void sendText(string serverResponse)
        {
            byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);

            clientSockel.Send(sendBytes);
            Console.WriteLine(serverResponse);
        }
        static void Main(string[] args)
        {
            while (true)
            {
                serverSocket.Start();
                Console.WriteLine("Server wurde gestartet ... wir lauschen");
                clientSockel = serverSocket.AcceptSocket();
                Console.WriteLine("Daten können entgegengenommen werden ....");
                Thread myThread = new Thread(run);
                myThread.Start();
                //myThread.Join();
                Console.WriteLine("Sever wurde gestoppt");
            }

            Console.ReadKey();



        }
    }
}
