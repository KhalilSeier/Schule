﻿using System;
using System.Threading;

namespace IncrementDecrement
{
    class Program
    {
        private static int counter = 0;
        static object locker = new object();
        static void Main(string[] args)
        {
            ThreadStart threadstart1 = new ThreadStart(Increment);
            Thread thread1 = new Thread(threadstart1);

            ThreadStart threadstart2 = new ThreadStart(Decrement);
            Thread thread2 = new Thread(threadstart2);

            thread1.Start();
            thread2.Start();

            Console.ReadKey();
        }
        public static void Increment()
        {
            while (true)
            {
                lock(locker)
                {
                    counter++;
                    Thread.Sleep(500);
                    Console.WriteLine(counter);
                }
            }
        }
        public static void Decrement()
        {
            while (true)
            {
                lock (locker)
                {
                    counter--;
                    Thread.Sleep(500);
                    Console.WriteLine(counter);
                }
            }
        }
    }
}
