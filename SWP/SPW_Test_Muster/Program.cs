﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace SPW_Test_Muster
{
    class Program
    {
        public class Server
        {
            Socket clientsocket;
            int clientID = 0;
            public Server(Socket cache_clientsocket, int id)
            {
                clientsocket = cache_clientsocket;
                clientID = id;
            }


            public void Run()
            {
                bool ueberpruefung = false;
                string einloggen = "einloggen";
                string erstellen = "erstellen";
                string abbrechen = "abbrechen";
                string beenden = "beenden";

                string einloggInformationen = string.Format("Möchten Sie sich einloggen oder einen Account erstellen?\n\n\r({3})Server > Um sich einzulogen schreiben Sie '{0}' um einen Account zu \n\rerstellen schreiben Sie '{1}' und um die Verbindung zu trennen schreiben \n\rSie '{2}'\n\n\r", einloggen, erstellen, beenden, clientID);
                do
                {
                    SendContent(einloggInformationen);
                    string action = GetContent().ToLower();
                    action = action.Substring(0, action.Length - 2);
                    if (action == einloggen)
                    {
                        bool einloggenErfolgreich = Einloggen();
                        if (einloggenErfolgreich)
                        {

                        }
                    }
                    else if (action == erstellen)
                    {
                        Erstellen();
                    }
                    else if (action == beenden)
                    {
                        ueberpruefung = true;
                    }
                    else
                    {
                        SendContent("Error!\n\r");
                    }

                } while (!ueberpruefung);

                SendContent("Verbindung wird nun beendet...");
                Thread.Sleep(3000);
                Console.WriteLine("Die Verbindung zum Client Nr. {0} wurde nun beendet", clientID);
                clientsocket.Close();
            }
            public string GetContent()
            {
                string content = string.Empty;
                while (true)
                {
                    Thread.Sleep(500);
                    if (clientsocket.Available > 0)
                    {
                        while (clientsocket.Available > 0)
                        {
                            int bytes = clientsocket.Available;
                            byte[] buffer = new byte[bytes];
                            clientsocket.Receive(buffer, bytes, SocketFlags.None);
                            content += Encoding.ASCII.GetString(buffer);
                        }
                        Console.WriteLine(string.Format("({1})Client < {0}", content, clientID));
                        return content;
                    }
                }
            }
            public void SendContent(string parameter)
            {
                byte[] message = Encoding.ASCII.GetBytes(string.Format("\n\rServer > {0}", parameter));
                clientsocket.Send(message);
                Console.WriteLine("({1})Server > {0}", parameter, clientID);
            }
            public bool Einloggen()
            {
                int i = 0;
                List<cls_Benutzer> listBenutzer;

                string abbrechen = "abbrechen";
                string messageU = string.Empty;
                string messageP = string.Empty;
                bool ueberpruefung = false;
                bool ueberpruefung2 = false;

                SendContent(string.Format("Bitte geben Sie Ihren Nutzernamen ein. Um abzubrechen geben Sie \n\r'{0}' ein\n\n\r", abbrechen));
                do
                {
                    messageU = GetContent();
                    messageU = messageU.Substring(0, messageU.Length - 2);
                    if (messageU == abbrechen)
                    {
                        return false;
                    }

                    else 
                    {
                        listBenutzer = cls_SQL_Connection.ReadData();
                        bool nutzernameVorhanden = false;
                        for(i = 0;i < listBenutzer.Count;i++)
                        {
                            if (messageU == listBenutzer[i].name)
                            {
                                SendContent("Benutzername Korrekt!\n\r");
                                ueberpruefung = true;
                                break;
                            }
                        }
                        if(!nutzernameVorhanden)
                        {
                            SendContent(string.Format("Benutername nicht Vorhanden! Bitte geben Sie ihren Benutzernamen erneut ein.\n\rUm abzubrechen geben Sie '{0}' ein\n\n\r", abbrechen));
                        }
                    }
                } while (!ueberpruefung);
                SendContent(string.Format("Bitte geben Sie nun ihren Password ein. Um abzubrechen geben Sie '{0}'ein\n\n\r", abbrechen));
                do
                {
                    messageP = GetContent();
                    messageP = messageP.Substring(0, messageP.Length - 2);
                    if (messageP == abbrechen)
                    {
                        return false;
                    }
                    else if (messageP == listBenutzer[i].password)
                    {

                        SendContent("Password Korrekt!");
                        ueberpruefung2 = true;
                    }
                    else
                    {
                        SendContent(string.Format("Password Falsch! Bitte geben Sie ihr Password erneut ein. Um abzubrechen geben Sie '{0}' ein\n\n\r", abbrechen));
                    }
                } while (!ueberpruefung2);
                SendContent("Einloggen erfolgreich!");
                SendContent(string.Format("Ihr Account wurde am {0} erstellt",listBenutzer[i].date));
                SendContent("Drücken Sie eine beliebige Taste um die Verbindung zu trennen");
                GetContent();
                return true;
            }
            public bool Erstellen()
            {
                int i;
                cls_Benutzer benutzer = new cls_Benutzer();
                List<cls_Benutzer> listBenutzer;
                
                string abbrechen = "abbrechen";
                string bestaetigen = "bestaetigen";
                string aendern = "aendern";
                string messageU = string.Empty;
                string messageP = string.Empty;
                string Ubestätigung = string.Empty;
                string Pbestätigung = string.Empty;
                bool ueberpruefungNutzername1;
                bool ueberpruefungNutzername2;
                bool ueberpruefungNutzername3;
                bool ueberpruefungPasswort1;
                bool ueberpruefungPasswort2;
                bool ueberpruefungPasswort3;

                do
                {

                    ueberpruefungNutzername1 = false;
                    ueberpruefungNutzername2 = false;
                    ueberpruefungNutzername3 = true;
                    ueberpruefungPasswort1 = false;
                    ueberpruefungPasswort2 = false;
                    ueberpruefungPasswort3 = false;
                    SendContent(string.Format("Bitte geben Sie einen Nutzernamen ein. Um abzubrechen geben Sie \n\r'{0}' ein\n\n\r", abbrechen));
                    do
                    {
                        messageU = GetContent();
                        messageU = messageU.Substring(0, messageU.Length - 2);
                        if (messageU == abbrechen)
                        {
                            return false;
                        }
                        else
                        {
                            bool nutzernameVorhanden = false;
                            listBenutzer = cls_SQL_Connection.ReadData();
                            for (i = 0; i < listBenutzer.Count; i++)
                            {
                                if (messageU == listBenutzer[i].name)
                                {
                                    nutzernameVorhanden = true;
                                    break;
                                }
                            }
                            if (!nutzernameVorhanden)
                               {
                                SendContent(string.Format("Ihr Benutzername ist {0}\n\r", messageU));
                                do
                                {
                                    SendContent(string.Format("Schreiben Sie '{0}' um Ihren Benutzernamen zu bestaetigen oder '{1}' um einen anderen Benutzernamen zu wählen. Um abzubrechen geben Sie \n\r'{2}' ein\n\n\r", bestaetigen, aendern, abbrechen));
                                    Ubestätigung = GetContent().ToLower();
                                    Ubestätigung = Ubestätigung.Substring(0, Ubestätigung.Length - 2);
                                    if (Ubestätigung == abbrechen)
                                    {
                                        ueberpruefungNutzername1 = true;
                                        ueberpruefungNutzername2 = true;
                                        ueberpruefungNutzername3 = false;
                                    }
                                    else if (Ubestätigung == bestaetigen)
                                    {
                                        ueberpruefungNutzername1 = true;
                                        ueberpruefungNutzername2 = true;
                                        benutzer.name = messageU;
                                    }
                                    else if (Ubestätigung == aendern)
                                    {
                                        ueberpruefungNutzername1 = true;
                                        ueberpruefungNutzername2 = true;
                                        ueberpruefungNutzername3 = false;
                                    }
                                    else
                                    {
                                        SendContent("Error!");
                                    }
                                } while (ueberpruefungNutzername1 == false);
                            }
                            else
                            {
                                SendContent("Error! Benutzername schon vergeben. Bitte wählen Sie einen anderen\n\rNutzernamen\n\rUm abzubrechen geben Sie 'abbrechen ein\n\n\r");
                            }
                        }
                    } while (ueberpruefungNutzername2 == false);
                    if (ueberpruefungNutzername3 == true)
                    {
                        do
                        {
                            ueberpruefungPasswort1 = false;
                            ueberpruefungPasswort2 = false;
                            ueberpruefungPasswort3 = false;
                            SendContent(string.Format("Bitte geben Sie einen Passwort ein. Um abzubrechen geben Sie \n\r'{0}' ein\n\n\r", abbrechen));
                            messageP = GetContent();
                            messageP = messageP.Substring(0, messageP.Length - 2);
                            if(messageP == abbrechen)
                            {
                                ueberpruefungPasswort1 = true;
                                ueberpruefungPasswort2 = true;
                            }
                            while(!ueberpruefungPasswort1)
                            {
                                SendContent(string.Format("Schreiben Sie Ihr Passwort erneut um es zu bestaetigen. Um abzubrechen geben Sie '{0}' ein\n\n\r", abbrechen));
                                Pbestätigung = GetContent();
                                Pbestätigung = Pbestätigung.Substring(0, Pbestätigung.Length - 2);
                                if (Pbestätigung == abbrechen)
                                {
                                    ueberpruefungPasswort2 = true;
                                    ueberpruefungPasswort1 = true;
                                }
                                else if (Pbestätigung == messageP)
                                {
                                    ueberpruefungPasswort1 = true;
                                    ueberpruefungPasswort2 = true;
                                    ueberpruefungPasswort3 = true;
                                    benutzer.password = messageP;
                                }
                                else if (Pbestätigung == aendern)
                                {
                                    ueberpruefungPasswort1 = true;
                                }
                                else
                                {
                                    SendContent("Falsches Passwort!");
                                }
                            }

                        } while (!ueberpruefungPasswort2);
                    }
                } while (!ueberpruefungPasswort3);
                benutzer.date = DateTime.Today;
                cls_SQL_Connection.WriteData(benutzer);
                SendContent(string.Format("Vielen Dank {0}. Ihr Account wurde erstellt. Drücken Sie Enter \n\rum fortzufahren", messageU));
                GetContent();
                //Datenbank oder datenbank returnen
                return true;
            }
            static void Main(string[] args)
            {
                int clientID = 1;
                string ipadress = "127.0.0.1";
                IPAddress iPAddress = IPAddress.Parse(ipadress);
                TcpListener tcpListener = new TcpListener(iPAddress, 27);
                tcpListener.Start();

                while (true)
                {
                    Socket client_socket = tcpListener.AcceptSocket();
                    Server server = new Server(client_socket, clientID);
                    ThreadStart threadStart = new ThreadStart(server.Run);
                    Thread thread = new Thread(threadStart);
                    thread.Start();
                    clientID++;
                }
            }
        }
    }
}
