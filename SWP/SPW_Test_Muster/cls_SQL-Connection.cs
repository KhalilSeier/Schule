﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;



namespace SPW_Test_Muster
{
    public static class cls_SQL_Connection
    {
        public const string CONNECTION_STRING = @"Server=localhost;Database=swp_muster;User=root;Pwd=;";
        public static void WriteData(cls_Benutzer benutzer)
        {
            using (MySqlConnection connection = new MySqlConnection(CONNECTION_STRING))
            {
                string sql = "Insert into u_p_d(date, user, password) values (@date, @user, @password)";
                MySqlCommand command = new MySqlCommand(sql, connection);
                command.Parameters.Add("@date", MySqlDbType.DateTime).Value = benutzer.date;
                command.Parameters.Add("@user", MySqlDbType.VarChar).Value = benutzer.name;
                command.Parameters.Add("@password", MySqlDbType.VarChar).Value = benutzer.password;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        public static List<cls_Benutzer> ReadData()
        {
            List <cls_Benutzer> benutzer = new List<cls_Benutzer>();
            cls_Benutzer b1 = new cls_Benutzer();
            benutzer.Add(b1);
            int i = 0;


            using (MySqlConnection connection = new MySqlConnection(CONNECTION_STRING))
            using (MySqlCommand command = new MySqlCommand("select * from u_p_d", connection))
            {
                connection.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        benutzer[i].name = reader.GetString(3);
                        benutzer[i].password = reader.GetString(1);
                        benutzer[i].date = reader.GetDateTime(2);
                    }
                }
            }
            return benutzer;
        }
    }
    public class cls_Benutzer
    {
        public string name
        {
            get;
            set;
        }
        public DateTime date
        {
            get;
            set;
        }
        public string password
        {
            get;
            set;
        }
    }
}
