﻿using System;
using System.Threading;

namespace Priority
{

    class PriorityTest
    {
        private bool loopSwitch = true;

        public bool Loopwitch
        {
            set { loopSwitch = value; }
        }
        public void run()
        {
            long threadcount = 0;

            while(loopSwitch)
            {
                threadcount++;
            }

            Console.WriteLine("{0} with {1,13} priority ... counter: {2,13}", Thread.CurrentThread.Name,
                Thread.CurrentThread.Priority.ToString(), threadcount);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 0;i<100;i++)
            {
                Run();
            }

            Console.ReadKey();

        }
        static void Run()
        {
            PriorityTest testobj = new PriorityTest();
            ThreadStart myDelegate = testobj.run;

            Thread threadOne = new Thread(myDelegate);
            threadOne.Name = "ThreadOne";
            threadOne.Priority = ThreadPriority.Highest;


            Thread threadTwo = new Thread(myDelegate);
            threadTwo.Name = "ThreadTwo";
            threadTwo.Priority = ThreadPriority.Lowest;

            threadOne.Start();
            threadTwo.Start();

            Thread.Sleep(100);

            testobj.Loopwitch = false;

        }
    }
}
