﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace dining_philosophers_problem
{
    enum DinerState { Eat, Get, Pon };
    class Fork
    {
        public int holder = -1;
    }
    class Diner
    {
        private static int instancecount;
        public Fork left;
        public Fork right;
        public int id;
        public DinerState state = new DinerState();
        const int maxWait = 100;
        public bool end = false;
        public Diner()
        {
            id = instancecount++;
            left = Program.forks[id];
            right = Program.forks[(id + 1) % Program.dinercount];
            state = DinerState.Get;

            Thread thread = new Thread(new ThreadStart(dostuff));
            thread.Start();
            
        }
        public void dostuff()
        {
            do
            {
                if (state == DinerState.Get)
                {
                    bool lockedR = false;
                    bool lockedL = false;
                    Monitor.TryEnter(left,ref lockedL);
                    if(lockedL)
                    {
                        left.holder = id;

                        Monitor.TryEnter(right, ref lockedR);
                        if (lockedR)
                        {
                            right.holder = id;
                            state = DinerState.Eat;
                            Thread.Sleep(Program.random.Next(0, maxWait));//Essen

                            right.holder = -1;
                            Monitor.Exit(right);

                            left.holder = -1;//?
                            Monitor.Exit(left);
                            state = DinerState.Pon;
                            Thread.Sleep(Program.random.Next(0, maxWait));
                        }
                        else
                        {
                            left.holder = -1;
                            Monitor.Exit(left);
                            Thread.Sleep(Program.random.Next(0, maxWait));
                        }
                    }
                    else
                    {
                        Thread.Sleep(Program.random.Next(0, maxWait));
                    }
                }
                else
                {
                    state = DinerState.Eat;
                }
            }
            while (!end) ;
        }
    }
    class Program
    {
        public const int dinercount = 5;
        public static List<Fork> forks = new List<Fork>();
        public static List<Diner> diners = new List<Diner>();
        static public int runSeconds = 60;

        public static Random random = new Random();



        static void Main(string[] args)
        {
            for(int i = 0;i<dinercount;i++) forks.Add(new Fork());
            for (int i = 0; i < dinercount; i++) diners.Add(new Diner());
            DateTime endTimer = DateTime.Now + new TimeSpan(0, 0, runSeconds);

            Console.Write("|");
            foreach (Diner d in diners) Console.Write("D"+d.id+" |");
            Console.Write("    |");
            for (int i = 0; i < dinercount; i++) Console.Write("F"+i+"|");
            Console.WriteLine();

            do
            {
                Console.Write("|");
                foreach (Diner d in diners) Console.Write(d.state + "|");
                Console.Write("    ");

                foreach(Fork f in forks)
                {
                    if (f.holder < 0) Console.Write("  |");
                    else Console.Write("D" + f.holder + "|");
                }

                Console.WriteLine();
                Thread.Sleep(1000);


            } while (DateTime.Now < endTimer);


            Console.ReadKey();

        }
    }
}
