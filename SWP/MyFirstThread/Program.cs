﻿using System;
using System.Threading;

namespace MyFirstThread
{
    class Program
    {
        static void Main(string[] args)
        {
            cls_Threading classThread = new cls_Threading();
            Console.WriteLine("Primärer Thread wurde gestartet main()...");
            //Thread thread = new Thread(classThread.Run);
            //thread.Start();
            ThreadStart firstthread;
            firstthread = new ThreadStart(classThread.Run);
            Thread TheThread = new Thread(firstthread);
            TheThread.Start();

            for(int i = 0; i < 100;i++)
            {
                Console.WriteLine("Prim. Thread Zähler: {0}", i);
                Thread.Sleep(50);
            }
            //TheThread.Abort();
            Console.ReadKey();
        }
    }
}
