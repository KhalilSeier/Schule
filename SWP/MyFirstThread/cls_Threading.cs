﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MyFirstThread
{
    class cls_Threading
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("Sekundärer Thread wurde gestartet...");
                for (int i = 0; i < 200; i++)
                {
                    Console.WriteLine("Sek. Thread Zähler: {0}", i);
                    Thread.Sleep(50);
                }
            }
            catch(ThreadAbortException exc)
            {
                Console.WriteLine("Sek. Thread ist im Catch Block");
                Console.WriteLine(exc.ToString());
            }
            finally
            {
                Console.WriteLine("Sek. Thread ist im Finally");
            }
        }
    }
}
