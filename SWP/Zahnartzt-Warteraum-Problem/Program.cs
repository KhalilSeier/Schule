﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Zahnartzt_Warteraum_Problem
{
    class Program
    {
        public static int m_Patient = -1;
        static int m_threadID = 0;
        static int numChairs = 2;

        public static Semaphore Wartezimmer = new Semaphore(numChairs, numChairs);

        public static Semaphore Zahnarztstuhl = new Semaphore(1, 1);        //Sessel  ist leer ...

        public static Semaphore Schlafsessel = new Semaphore(0, 1);         //Zahnarzt schläft

        public static Semaphore Seatbelt = new Semaphore(0, 1);

        public class mySemiphores
        {
            int threadID;
            public mySemiphores()
            {
                threadID = m_threadID;
                m_threadID++;
            }
            public void Patient()
            {
                while (true)
                {
                    Thread.Sleep(400);
                    Wartezimmer.WaitOne();
                    Console.WriteLine("Patient {0} ist im Wartezimmer", threadID);
                    Zahnarztstuhl.WaitOne();
                    Console.WriteLine("Patient {0} ist im Zahnartztstuhl", threadID);
                    Wartezimmer.Release();
                    m_Patient = threadID;
                    Schlafsessel.Release();
                    Thread.Sleep(400);
                    Seatbelt.WaitOne();
                }
            }
            public void Zahnartzt()
            {
                while (true)
                {
                    Schlafsessel.WaitOne();
                    Console.WriteLine("Patient {0} wird behandelt", m_Patient);
                    Thread.Sleep(400);
                    Console.WriteLine("Patient {0} wird entlassen", m_Patient);
                    Zahnarztstuhl.Release();
                    Seatbelt.Release();
                }
            }
        }
        

        static void Main(string[] args)
        {

            mySemiphores semiphores1 = new mySemiphores();
            mySemiphores semiphores2 = new mySemiphores();
            mySemiphores semiphores3 = new mySemiphores();
            mySemiphores semiphores4 = new mySemiphores();
            mySemiphores semiphores5 = new mySemiphores();
            mySemiphores semiphores6 = new mySemiphores();

            Thread thread1, thread2, thread3, thread4, thread5, thread6;

            thread1 = new Thread(new ThreadStart(semiphores1.Patient));
            thread2 = new Thread(new ThreadStart(semiphores2.Patient));
            thread3 = new Thread(new ThreadStart(semiphores3.Patient));
            thread4 = new Thread(new ThreadStart(semiphores4.Patient));
            thread5 = new Thread(new ThreadStart(semiphores5.Patient));
            thread6 = new Thread(new ThreadStart(semiphores6.Zahnartzt));

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            thread5.Start();
            thread6.Start();

            Console.ReadKey();


        }
    }
}
