﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeChecker
{
    class Program
    {
        static bool CheckPrim(int number)
        {
            if (number <= 1) return false;
            for(int i  = 2; i < Math.Sqrt(number); i++)
            {
                if (number % i == 0) return false;
            }
            return true;
        }


        static void Main(string[] args)
        {
            for(int i = 0; i < 100; i++)
            {
                if(CheckPrim(i))
                {
                    Console.WriteLine("prime: {0}", i);
                }
            }

            Console.ReadKey();
        }
    }
}
