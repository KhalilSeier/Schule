﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

namespace PrimeCheckerEvent
{
    public class PrimeChecker
    {
        public delegate void PrimeDel(object prime,PrimeCheckerEventArks e);
        public event PrimeDel PrimeEvent;

        static bool CheckPrim(int number)
        {
            if (number <= 1) return false;
            for (int i = 2; i < Math.Sqrt(number); i++)
            {
                if (number % i == 0) return false;
            }
            return true;
        }

        public void Process()
        {
            int i = 0;
            while (true)
            {
                if (CheckPrim(i))
                {
                    // Ereigniss
                    PrimeCheckerEventArks prime = new PrimeCheckerEventArks(i);
                    OnPrime(this,prime);
                }
                i++;
                Thread.Sleep(200);
            }
        }

        private void OnPrime(object prime,PrimeCheckerEventArks e)
        {
            if (PrimeEvent != null)
                PrimeEvent(this,e);
        }
    }

    class Program
    {
        static void printPrime(object prime,PrimeCheckerEventArks e)
        {
            Console.WriteLine(e.prime);
        }

        static void Main(string[] args)
        {
            PrimeChecker pc = new PrimeChecker();
            pc.PrimeEvent += new PrimeChecker.PrimeDel(printPrime);

            pc.Process();

            Console.ReadKey();
        }
    }
}
