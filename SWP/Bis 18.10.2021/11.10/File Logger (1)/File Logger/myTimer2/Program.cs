﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

namespace myTimerEvent
{
    public class TimerInfoArgs : EventArgs
    {
        public readonly int hour;
        public readonly int minute;
        public readonly int second;

        public TimerInfoArgs(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }
    }
    
    public class Zeitgeber
    {
        public delegate void ZeitgeberDel(object clock,TimerInfoArgs timerInfo);
        public event ZeitgeberDel ZeitgeberEvent;

        int oldSecond;

        public void OnSecondChanged(object clock, TimerInfoArgs timerInfo)
        {
            if(ZeitgeberEvent != null)
            {
                ZeitgeberEvent(clock, timerInfo);
            }
        }

        public void Run()
        {
            while(true) // until doomsday
            {
                Thread.Sleep(10);
                DateTime dt = DateTime.Now;

                if(dt.Second != oldSecond)
                {
                    //Ereigniss ereignet sich hier
                    TimerInfoArgs timerInfo = new TimerInfoArgs(dt.Hour, dt.Minute, dt.Second);



                    OnSecondChanged(this, timerInfo);
                    oldSecond = dt.Second;
                }
            }
        }
    }

    public class DisplayClock
    {
        public void Subscribe(Zeitgeber clock)
        {
            clock.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(TimeHasChanged);
        }

        public void TimeHasChanged(object clock, TimerInfoArgs e)
        {
            Console.WriteLine("Current time: {0}:{1}:{2}", e.hour,e.minute,e.second);
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            Zeitgeber myClock = new Zeitgeber();
            DisplayClock dc = new DisplayClock();
            dc.Subscribe(myClock);
            myClock.Run();

            Console.ReadKey();
        }
    }
}
