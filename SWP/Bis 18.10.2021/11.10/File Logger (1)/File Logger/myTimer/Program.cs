﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.IO;

namespace myTimer
{
    public class Zeitgeber
    {
        public delegate void ZeitgeberDel(string message);
        public event ZeitgeberDel ZeitgeberEvent;

        public void Process()
        {
            while(true)
            {
                string msg = String.Format("Time: {0}", DateTime.Now);
                //Ereignis soll ausgelöst werden ...
                OnLog(msg);
                Thread.Sleep(1000);
            }
        }

        private void OnLog(string message)
        {
            if (ZeitgeberEvent != null)
                ZeitgeberEvent(message);
        }



    }
    
    
    class Program
    {
        static FileStream fileStream;
        static StreamWriter streamWriter;

        static void timerFunction(string s)
        {
            Console.WriteLine(s);
        }

        static void timerFunction2(string s)
        {
            Program.fileStream = new FileStream("file.txt", FileMode.Append);
            Program.streamWriter = new StreamWriter(fileStream);
            streamWriter.WriteLine(s);
            streamWriter.Close();
            fileStream.Close();
        }


        static void Main(string[] args)
        {
            Zeitgeber timer = new Zeitgeber();
            timer.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(timerFunction);
            timer.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(timerFunction2);

            timer.Process();
        }
    }
}
