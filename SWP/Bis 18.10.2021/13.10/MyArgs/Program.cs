﻿using System;

namespace MyArgs
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args == null)
            {
                Console.WriteLine("Es wurden keine Parameter übergeben");
            }
            else
            {
                int argsCount = args.Length;
                Console.WriteLine("Es wurden {0} AParamenter übergeben.", argsCount);
            }
            
        }
    }
}
