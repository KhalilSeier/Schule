﻿using System;

namespace Durchschnitt
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args != null)
            {
                int minus = 0;
                double summe = 0;
                foreach(string a in args)
                {
                    Console.WriteLine(a);
                    if(double.TryParse(a, out double result))
                    {
                        summe += result;
                    }
                    else
                    {
                        minus++;
                    }
                }
                Console.WriteLine();
                Console.WriteLine(Convert.ToDouble(summe/(args.Length-minus)));
            }
            Console.ReadKey();
        }
    }
}
