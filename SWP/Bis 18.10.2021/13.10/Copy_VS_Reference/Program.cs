﻿using System;

namespace Copy_VS_Reference
{
    class Program
    {
        static void SumProd(ref int a, ref int c,out int b, out int d)
        {
            b = a + c;
            d = a * c;
        }
        static int f1(int x) // Die 3  wird hier her kopiert
        {
            return 3 * x;
        }
        static int f2(ref int x) // Hier wird die Adresse des Speicherplatz von x übergeben
        {
            return 3 * x;
        }
        static void f3(ref int x, out int y)
        {
            y = 3 * x;
        }
        static void Main(string[] args)
        {
            int c;
            int d;
            int x = 3;
            int y = 0;
            Console.WriteLine("f1(3)={0}", f1(3));

            Console.WriteLine("f2(3)={0}", f2(ref x));

            f3(ref x, out y);
            Console.WriteLine("f2(3)={0}", y);

            SumProd(ref x, ref x, out c, out d);

            Console.WriteLine("Summe: {0}",c);
            Console.WriteLine("Produkt: {0}",d);
            Console.ReadKey();
        }
    }
}
