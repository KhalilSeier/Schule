﻿using System;

using System.Threading;
using System.IO;

namespace MyTimer
{
    class Program
    {
        static void Main(string[] args)
        {
            Zeitgeber timer = new Zeitgeber();
            timer.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(timerFunction);
            timer.ZeitgeberEvent += new Zeitgeber.ZeitgeberDel(timerFunction2);
            timer.Process();
        }
        static void timerFunction(string s)
        {
            Console.WriteLine(s);
        }
        static void timerFunction2(string s)
        {
            //Console.WriteLine(s);
            //Console.WriteLine("--------------------------");
            WriteOrReadToFile.WriteinFile(s);
        }

    }

    public class Zeitgeber
    {
        public delegate void ZeitgeberDel(string message);
        public event ZeitgeberDel ZeitgeberEvent;
        public void Process()
        {
            while (true)
            {
                string msg = String.Format("Time: {0}", DateTime.Now);
                //Ereigniss soll ausgelößt werden ...
                OnLog(msg);
                Thread.Sleep(1000);
            }
        }

        private void OnLog(string message)
        {
            if (ZeitgeberEvent != null)
            {
                ZeitgeberEvent(message);
            }
        }
    }
    public static class WriteOrReadToFile
    {
        static FileStream fileStream;
        static StreamWriter streamWriter;
        public static void WriteinFile(string s)
        {
            fileStream = new FileStream("file.txt", FileMode.Append);
            streamWriter = new StreamWriter(fileStream);
            streamWriter.WriteLine(s);
            streamWriter.Close();
            fileStream.Close();
        }
    }
}
