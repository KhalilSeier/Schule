﻿using System;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Reflection;


namespace FunctionVsDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
                Stopwatch sw = Stopwatch.StartNew();
                MyWrapperFunct myWrapper = new MyWrapperFunct();
                const int iterations = 100000;

                for (int i = 0; i < iterations; i++)
                {
                    myWrapper.SomepublicMethode();
                }

                sw.Stop();
                Console.WriteLine("Function call: {0}ms", sw.ElapsedMilliseconds);
            Console.ReadKey();
        }
        public class ThirdPartyComponent
        {
            static private void SomePrivateMethode()
            {
                //do nothing
            }
        }
        public class MyWrapperFunct
        {
            private static MethodInfo myMethod = typeof(ThirdPartyComponent).GetMethod("SomePrivateMethode", BindingFlags.NonPublic | BindingFlags.Static);
            
            public void SomepublicMethode()
            {
                myMethod.Invoke(null,null);
            }
        }
        public class MyWrapperDelegate
        {
            private static MethodInfo myMethod = typeof(ThirdPartyComponent).GetMethod("SomePrivateMethode", BindingFlags.NonPublic | BindingFlags.Static);

            public delegate void MyDelegate();

            public static MyDelegate myDelegate = (MyDelegate)Delegate.CreateDelegate(typeof(MyDelegate), myMethod);

            public void SomepublicMethode()
            {
                myDelegate();
            }
        }
    }
}
