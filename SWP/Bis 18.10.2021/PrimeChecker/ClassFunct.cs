﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Numerics;

namespace PrimeChecker
{
    class ClassFunct
    {
        public static StreamWriter streamWriter;
        public static FileStream fileStream;
        const string filename = "primzahl.txt";
        public void Process(BigInteger i)
        {
            WriteInTextfile(string.Format("start: {0}", i));
            bool prime = false;
            while (!prime)
            {
                if (CheckPrim(i))
                {
                    Console.WriteLine("prime: {0}", i);
                    WriteInTextfile(string.Format("prime: {0}", i));
                    prime = true;
                }
                i++;
            }
        }
        public void WriteInTextfile(string output)
        {
            if (!File.Exists(filename))
            {
                fileStream = new FileStream(filename, FileMode.Create);
                fileStream.Close();
            }
            fileStream = new FileStream(filename, FileMode.Append);
            streamWriter = new StreamWriter(fileStream);

            streamWriter.WriteLine(output);
            streamWriter.Close();
        }
        static bool CheckPrim(BigInteger number)
        {
            //if (number <= 1) return false;
            for (BigInteger i = 2; (double)i < Math.Sqrt((double)number); i++)
            {
                if (number % i == 0) return false;
            }
            return true;
        }
    }
}
