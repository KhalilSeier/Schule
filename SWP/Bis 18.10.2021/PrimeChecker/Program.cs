﻿using System;
using System.Numerics;
using System.IO;
namespace PrimeChecker
{
    class Program
    {

        static void Main(string[] args)
        {

            ClassFunct classFunct = new ClassFunct();
            classFunct.WriteInTextfile(string.Format("Start des Duchlaufes am {0} \n\r", DateTime.Now.ToString()));
            BigInteger getnumber = 50;
            while (true)
            {
                getnumber = getnumber * 13;
                Console.WriteLine(string.Format(string.Format("start: {0}",getnumber.ToString())));
                BigInteger i = getnumber;
                classFunct.Process(i);
            }
            Console.ReadKey();

        }




    }
}
