﻿using System;
using System.IO;

namespace FilesLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            Fileloger fl = new Fileloger("process.log");

            MyClass.LogHandler handlerDel = null;
            handlerDel += fl.Logger;
            handlerDel += fl.Logger2;

            MyClass myClass = new MyClass();
            myClass.Process(handlerDel);
            fl.Close();
            Console.ReadKey();

        }
        class Fileloger
        {
            FileStream filestream;
            StreamWriter streamWriter;
            public Fileloger(string filename)
            {
                filestream = new FileStream(filename,FileMode.Create);
                streamWriter = new StreamWriter(filestream);
            }

            public void Logger(string s)
            {
                DateTime dateTime = DateTime.Now;
                streamWriter.Write(dateTime.ToString()+"   ");
                streamWriter.WriteLine(s);

            }
            public void Logger2(string s)
            {
                DateTime dateTime = DateTime.Now;
                Console.Write(dateTime.ToString()+"   ");
                Console.WriteLine(s);


            }

            public void Close()
            {
                streamWriter.Close();
                filestream.Close();
            }

        }
        class MyClass
        {
            public delegate void LogHandler(string message);
            public void Process(LogHandler logHandler)
            {
                if(logHandler != null)
                {
                    logHandler("Process has been started");
                    //......
                    logHandler("......Process has been stopped");
                }
            }
        }
    }
}
