﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ErsteMitarbeit
{
 
    class Program
    {
        class _Threads
        {
            public bool availabel = false;
            public bool availabelPrimzahl = false;
            public int zahl;
            public int primzahl;
            public object mon1 = new object();
            public object mon2 = new object();
            Random random = new Random();
            public void Producer()
            { 
                while(true)
                {
                    if(!availabel)
                    {
                        //Thread.Sleep(300);
                        Monitor.Enter(mon2);
                        zahl = random.Next(0, 100);
                        Console.WriteLine("Producer: {0}", zahl);
                        availabel = true;
                        Monitor.Exit(mon2);
                    }
                }
            }
            public void Checker()
            {
                while (true)
                {
                    if (availabel)
                    {
                        //Thread.Sleep(300);
                        Monitor.Enter(mon2);
                        if(IsPrime(zahl))
                        {
                            Monitor.Enter(mon1);
                            primzahl = zahl;
                            availabelPrimzahl = true;
                            Console.WriteLine("Checker: {0} is Prime",zahl);
                            Monitor.Exit(mon1);
                        }
                        else
                        {
                            Console.WriteLine("Checker: {0} is not Prime", zahl);
                        }
                        availabel = false;
                        Monitor.Exit(mon2);
                    }
                }
            }
            public void Consumer()
            {
                while (true)
                {
                    if (availabelPrimzahl)
                    {
                        //Thread.Sleep(300);
                        Monitor.Enter(mon1);
                        Console.WriteLine("Consumer: {0}", primzahl);
                        availabelPrimzahl = false;
                        Monitor.Exit(mon1);
                    }
                }
            }
            public bool IsPrime(int number)
            {
                if (number <= 1) return false;
                if (number == 2) return true;
                if (number % 2 == 0) return false;

                var boundary = (int)Math.Floor(Math.Sqrt(number));

                for (int i = 3; i <= boundary; i += 2)
                    if (number % i == 0)
                        return false;

                return true;
            }

        }
        static void Main(string[] args)
        {
            _Threads threads = new _Threads();
            Thread producer, checker, consumer;

            producer = new Thread(new ThreadStart(threads.Producer));
            checker = new Thread(new ThreadStart(threads.Checker));
            consumer = new Thread(new ThreadStart(threads.Consumer));

            producer.Start();
            checker.Start();
            consumer.Start();
        }
    }
}
