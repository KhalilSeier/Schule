﻿using System;

using System.Threading;

namespace ProducerKonsumer
{
    class Program
    {
        public static Object obj = new Object();
        public static bool avaiabel = false;
        public static int counter = 0;

        public static void Produce()
        {
                while (true)
                {
                    if (!avaiabel)
                    {
                        Thread.Sleep(1000);
                        Monitor.Enter(obj);
                        Random random = new Random();
                        counter = random.Next(69, 420);
                        Console.WriteLine("Producer: {0}",counter);
                        avaiabel = true;
                        Monitor.Exit(obj);
                    }
                }

        }
        public static void Consume()
        {
                while (true)
                {
                    if (avaiabel)
                    {
                        Thread.Sleep(10);
                        Monitor.Enter(obj);
                        Console.WriteLine("Consumer: {0}",counter);
                        avaiabel = false;
                        Monitor.Exit(obj);
                    }
                }
        }
        static void Main(string[] args)
        {
            Thread thread1, thread2;

            thread1 = new Thread(new ThreadStart(Produce));
            thread2 = new Thread(new ThreadStart(Consume));

            thread1.Start();
            thread2.Start();
        }
    }
}
