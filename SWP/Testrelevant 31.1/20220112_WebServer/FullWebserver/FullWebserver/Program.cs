﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;

namespace FullWebserver
{
    class Program
    {
        static void Main(string[] args)
        {


            string ipAdresse = "127.0.0.1";
            IPAddress remoteAdresse = IPAddress.Parse(ipAdresse);
            TcpListener server = new TcpListener(remoteAdresse,8080);
            server.Start();
            while (true)
            {
                Socket sock = server.AcceptSocket(); // da warten wir bis jemand zu uns eine verbindung aufbaut blokieren
                clsSeverhandler handle = new clsSeverhandler(sock);
                Thread clientThread = new Thread(new ThreadStart(handle.run));
                clientThread.Start();
            }
            Console.ReadKey();
        }
    }
}
