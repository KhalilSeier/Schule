﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.IO;
using System.Text;

namespace FullWebserver
{
    class clsSeverhandler
    {
        private Socket socket;
        public clsSeverhandler(Socket socket)
        {
            this.socket = socket;
        }
        public void run()
        {
            Thread.Sleep(500);
            string request = getContent();
            try
            {
                Console.WriteLine("-------------------------");
                Console.WriteLine(request);
                Console.WriteLine("-------------------------");

                string[] lines = request.Split(Environment.NewLine.ToCharArray()); // aufteilen eines strings
                string[] words = lines[0].Split(' ');

                if (words[0] == "GET")
                {
                    int pos = words[1].LastIndexOf(@"/");
                    if (pos > -1)
                    {
                        if (words[1] != "/" && words[1].Length > pos)
                        {
                            // file name auslesen und file zurückschicken.....
                            string filename = words[1].Substring(pos + 1);
                            if (File.Exists(Environment.CurrentDirectory + @"\files\" + filename))
                                sendFile(filename);
                            else sendResponse("404 NOT FOUND", "Datei nicht gefunden");
                        }
                        else
                        {
                            // aus den rout verzeichnis Standardseite zurückschicken......
                            if (File.Exists(Environment.CurrentDirectory + @"\files\index.html"))
                                sendFile(Environment.CurrentDirectory + @"\files\index.html");
                            else sendResponse("404 NOT FOUND", "Datei nicht gefunden");
                        }
                    }
                }
                else
                {
                    sendResponse("400 BAD REQUEST","Befehl wird nicht unterstütz");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void sendFile(String filename)
        {
            //FileStream stream = new FileStream(Environment.CurrentDirectory + @"\files\" + filename, FileMode.Open);
            FileStream stream = new FileStream(filename, FileMode.Open);
            //LogConsole("200 OK [ " + filename + " ]");
            sendText("HTTP/1.1 200 OK");
            sendText("Server: CLWebserver 1.00");
            sendText("Connection: close");
            sendText("Content-Type: text/html");
            sendText("Content-Length: " + stream.Length);
            sendText("");
            long read = 0;
            while (read < stream.Length)
            {
                int buffersize;
                if (stream.Length - read > 1024)
                {
                    buffersize = 1024;
                    read += 1024;
                }
                else
                {
                    buffersize = (int)(stream.Length - read);
                    read = stream.Length;
                }
                byte[] tosend = new byte[buffersize];
                stream.Read(tosend, 0, buffersize);
                socket.Send(tosend);
                Thread.Sleep(1);
            }
            stream.Close();
        }

        private void sendResponse(string status, string toSend)
        {
            sendText(@"HTTP/1.1.+" + status);
            sendText(@"Server: Mein Server V1.0");
            sendText(@"Connection: close");
            sendText(@"Content-Type: text/html");
            sendText(@"Content-Length =" + (toSend.Length + Environment.NewLine.Length));
            sendText(@" ");
            sendText(toSend);
        }

        private void sendText(string text)
        {
            socket.Send(Encoding.ASCII.GetBytes(text + Environment.NewLine));
        }
        private string getContent()
        {
            string content = string.Empty;
            while (socket.Available > 0)
            {
                Thread.Sleep(10);
                int bytes = socket.Available;
                byte[] buffer = new byte[bytes];
                socket.Receive(buffer, SocketFlags.None);
                content += Encoding.ASCII.GetString(buffer);

            }

            return content;
        }
    }
}
