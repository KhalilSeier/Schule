﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SMTP
{
    public class clsConnection
    {
        public Socket clientSocket;
        enum states { closed, begin, wait, envelope_created, recipients_set, writing_mail, ready_to_deliver};
        states currentState = states.closed;


        public clsConnection(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
            currentState = states.begin;
        }

        public void send(string message)
        {
            byte[] sendBytes = Encoding.ASCII.GetBytes(message + Environment.NewLine);
            clientSocket.Send(sendBytes);
        }

        public void run()
        {
            while(true)// until doomsday
            {
                if (clientSocket.Available != 0)
                {
                    try
                    {
                        int bytes = clientSocket.Available;
                        byte[] buffer = new byte[bytes];
                        clientSocket.Receive(buffer, bytes, SocketFlags.None);   
                        string received = Encoding.ASCII.GetString(buffer);

                        if (received != Environment.NewLine)
                        {

                            Console.WriteLine("<< {0}", received);

                            //state machine
                            if (currentState == states.begin && received.Contains("EHLO"))
                            {
                                send("250 OK");
                                currentState = states.wait;
                            }

                            if (currentState == states.wait && received.Contains("MAIL FROM:"))
                            {
                                send("250 OK");
                                currentState = states.envelope_created;
                            }

                            if (currentState == states.envelope_created && received.Contains("RCPT TO:"))
                            {
                                send("250 OK");
                                currentState = states.recipients_set;
                            }

                            if (currentState == states.envelope_created && received.Contains("RSET"))
                            {
                                send("250 OK");
                                currentState = states.wait;
                            }

                            if (currentState == states.recipients_set && received.Contains("RSET"))
                            {
                                send("250 OK");
                                currentState = states.wait;
                            }

                            if (currentState == states.recipients_set && received.Contains("DATA"))
                            {
                                send("250 OK");
                                currentState = states.writing_mail;
                            }

                            if (currentState == states.writing_mail && received != ".")
                            {
                                send(received);
                            }

                            if (currentState == states.writing_mail && received == ".")
                            {
                                send("250 OK");
                                currentState = states.wait;
                            }

                            Console.WriteLine("state: {0}", currentState);
                        }
                    }
                    catch(Exception exc)
                    {
                        Console.WriteLine(exc);
                    }

                }
            }
        }
    }

    class Program
    {
        static string ipAddress = "127.0.0.1";
        static IPAddress remoteAddress = IPAddress.Parse(ipAddress);

        static TcpListener serverSocket = new TcpListener(remoteAddress, 25);
        static Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        static List<clsConnection> myConnections = new List<clsConnection>();

        static string dataFromClient = String.Empty;
        static string sendMessage = String.Empty;

        

        static void Main(string[] args)
        {
            serverSocket.Start(); // Server geht in den LISTEN Zustand 
            Thread myThread;

            Console.WriteLine(">> SMTP-Server wurde gestartet.");

            while (true)
            {
                clientSocket = serverSocket.AcceptSocket(); // wir warten auf einen Verbindungsaufbau (SYN-Paket)

                clsConnection newConnection = new clsConnection(clientSocket);
                myConnections.Add(newConnection);

                myThread = new Thread(newConnection.run);
                myThread.Start();
            }


        }
    }
}
