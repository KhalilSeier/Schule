﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace RemoteShell
{
    class Program
    {
        static string ipAddress = "127.0.0.1";
        static IPAddress remoteAddress = IPAddress.Parse(ipAddress);
        static TcpListener serverSocket = new TcpListener(remoteAddress, 23); //telnet!!!
        static Socket clienetSocket;
        static Process myProcess = new Process();
        static string cmdString;

        public static void sendText(string response)
        {
            byte[] sendBytes = Encoding.ASCII.GetBytes(response);
            clienetSocket.Send(sendBytes);
        }

        static void run()
        {
            while (true) //until doomsday
            {
                int bytes = clienetSocket.Available;
                byte[] buffer = new byte[bytes];

                clienetSocket.Receive(buffer, bytes, SocketFlags.None);

                string dataFromClient = Encoding.ASCII.GetString(buffer);
                cmdString = dataFromClient.Trim();

                Console.WriteLine(cmdString);

                myProcess.StartInfo.FileName = "cmd.exe";
                myProcess.StartInfo.Arguments = " /C " + cmdString;
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.RedirectStandardError = true;
                myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.Start();

                string output = myProcess.StandardOutput.ReadToEnd();

                Console.WriteLine(output);
                sendText(output + Environment.NewLine + ">");
            }
        }

        static void Main(string[] args)
        {
            serverSocket.Start();
            clienetSocket = serverSocket.AcceptSocket();

            Thread myThread = new Thread(run);
            myThread.Start();
            myThread.Join();

        }
    }
}
