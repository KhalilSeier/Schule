﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace XMLsimple
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create the XmlDocument.
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(("<Student type='regular' Section='B'><Name>Tommy ex</Name></Student>")); 
            //Save the document to a file.
            doc.Save("C:\\std.xml"); 
            //You can also use Save method to display contents on console if you pass Console.Out as a 
            //parameter. For example: 
            doc.Save(Console.Out);

            Console.ReadLine();
        }
    }
}
