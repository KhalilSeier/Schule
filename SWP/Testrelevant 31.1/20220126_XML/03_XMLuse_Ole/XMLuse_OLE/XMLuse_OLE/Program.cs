﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb; 

namespace XMLuse_OLE
{
    class Program
    {
        static void Main(string[] args)
        {
            // create a connection 
            OleDbConnection con = new OleDbConnection();

            con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Northwind.mdb";

            // create a data adapter
            OleDbDataAdapter da = new OleDbDataAdapter("Select * from Customers", con);

            // create a new dataset
            DataSet ds = new DataSet();

            // fill dataset
            da.Fill(ds, "Customers");

            // write dataset contents to an xml file by calling WriteXml method
            ds.WriteXml("OutputXML.xml"); 

        }
    }
}
