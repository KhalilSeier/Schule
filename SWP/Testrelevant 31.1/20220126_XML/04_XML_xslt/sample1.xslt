<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
    <b>Name of Employee : </b><xsl:value-of select="Employees/Employee/Name" /><br/>
    <b>Age:</b><xsl:value-of select="Employees/Employee/Age" /><br/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>