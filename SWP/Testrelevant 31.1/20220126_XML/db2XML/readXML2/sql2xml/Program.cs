﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Xml;

namespace sql2xml
{
    class Program
    {
        static void Main(string[] args)
        {

            // create a connection
            MySqlConnection con = new MySqlConnection();
            con.ConnectionString = "Server=localhost;Database=northwind;Uid='root';Pwd='';";
            // create a data adapter
            MySqlDataAdapter da = new MySqlDataAdapter("Select * from Customers", con);
            // create a new dataset
            DataSet ds = new DataSet();
            // fill dataset
            da.Fill(ds, "Customers");
            // write dataset contents to an xml file by calling WriteXml method
            ds.WriteXml("OutputXML.xml");

        }
    }
}
