﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Xml.Xsl;

// taken from:
// http://www.aspfree.com/c/a/XML/Applying-XSLT-to-XML-Using-ASP.NET/3/

namespace Xstl
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument docXML = new XmlDocument();
            //docXML.Load(Server.MapPath("sample1.xml"));
            docXML.Load(Server.MapPath("Hamlet.xml"));
            XslTransform docXSL = new XslTransform(); 
            //docXSL.Load(Server.MapPath("sample1.xslt"));
            docXSL.Load(Server.MapPath("willy.xsl"));
            Xml1.Document = docXML;
            Xml1.Transform = docXSL;        
        }
    }
}