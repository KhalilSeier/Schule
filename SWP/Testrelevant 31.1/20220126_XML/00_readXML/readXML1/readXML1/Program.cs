﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace readXML1
{
    class Program
    {
        static void Main(string[] args)
        {
            int ws = 0;
            int pi = 0;
            int dc = 0;
            int cc = 0;
            int ac = 0;
            int et = 0;
            int el = 0;
            int xd = 0;
            // Read a document
            XmlTextReader textReader = new XmlTextReader("books.xml");
            // Read until end of file
            while (textReader.Read())
            {
                XmlNodeType nType = textReader.NodeType;

                switch (nType)
                {
                    case XmlNodeType.Text:
                        // if node type is a text
                        Console.WriteLine("text:       " + textReader.Value);
                        break;
                    case XmlNodeType.XmlDeclaration:
                        // If node type us a declaration
                        Console.WriteLine("Declaration:" + textReader.Name.ToString());
                        xd = xd + 1;
                        break;
                    case XmlNodeType.Comment:
                        // if node type is a comment
                        Console.WriteLine("Comment:    " + textReader.Name.ToString());
                        cc = cc + 1;
                        break;
                    /*case XmlNodeType.Attribute:
                        // if node type us an attribute
                        Console.WriteLine("Attribute:" + textReader.Name.ToString());
                        ac = ac + 1;
                        break;*/
                    case XmlNodeType.Element:
                        // if node type is an element
                        int i = 0;
                        i = textReader.AttributeCount;
                        if (i>0) Console.WriteLine("Element:" + textReader.Name.ToString() + "     Attribute-Value:" + textReader.GetAttribute(i-1));
                        else Console.WriteLine("Element:" + textReader.Name.ToString());
                        el = el + 1;
                        break;
                    case XmlNodeType.Entity:
                        // if node type is an entity
                        Console.WriteLine("Entity:" + textReader.Name.ToString());
                        et = et + 1;
                        break;
                    case XmlNodeType.ProcessingInstruction :
                        // if node type is a Process Instruction
                        Console.WriteLine("Instruction:" + textReader.Name.ToString());
                        pi = pi + 1;
                        break;
                    case XmlNodeType.DocumentType:
                        // if node type a document
                        Console.WriteLine("Document:" + textReader.Name.ToString());
                        dc = dc + 1;
                        break;
                    case XmlNodeType.Whitespace:
                        // if node type is white space
                        //Console.WriteLine("WhiteSpace:" + textReader.Name.ToString());
                        ws = ws + 1;
                        break;
                    default:
                        //Console.WriteLine("unknown element");
                        break;
                }
                
            }
            // Write the summary
            Console.WriteLine("Total Comments:" + cc.ToString());
            Console.WriteLine("Total Attributes:" + ac.ToString());
            Console.WriteLine("Total Elements:" + el.ToString());
            Console.WriteLine("Total Entity:" + et.ToString());
            Console.WriteLine("Total Process Instructions:" + pi.ToString());
            Console.WriteLine("Total Declaration:" + xd.ToString());
            Console.WriteLine("Total DocumentType:" + dc.ToString());
            Console.WriteLine("Total WhiteSpaces:" + ws.ToString());

            Console.ReadLine();
        }
    }
}
