﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace writeXML
{
    class Program
    {
        static void Main(string[] args)
        {/*
            <?xml version="1.0"?>
            <!--First Comment XmlTextWriter Sample Example-->
            <!--myXmlFile.xml in root dir-->
            <Student>
                <r:RECORD xmlns:r="urn:record">
                    <Name>Student</Name>
                    <Address>Colony</Address>
                    <Char>arc</Char>
                </r:RECORD>
            </Student>
           */

            // Create a new file in C:\\ dir
            XmlTextWriter textWriter = new XmlTextWriter("myXmFile.xml", null);
            // Opens the document
            textWriter.WriteStartDocument();
            // Write comments
            textWriter.WriteComment("First Comment XmlTextWriter Sample Example");
            textWriter.WriteComment("myXmlFile.xml in root dir");
            // Write first element
            textWriter.WriteStartElement("Student");
            textWriter.WriteStartElement("RECORD", "urn:record");
            // Write next element
            textWriter.WriteStartElement("Name", "");
                textWriter.WriteString("Sepp");
            textWriter.WriteEndElement();
            // Write one more element
            textWriter.WriteStartElement("Address", "");
                textWriter.WriteString("Dr.-Karl-Widdmann-Straße 40");
            textWriter.WriteEndElement();
            // WriteChars
            char[] ch = new char[3];
            ch[0] = 'a';
            ch[1] = 'r';
            ch[2] = 'c';
            textWriter.WriteStartElement("Char");
                textWriter.WriteChars(ch, 0, ch.Length);
            textWriter.WriteEndElement();
            // Ends the document.
            textWriter.WriteEndDocument();
            // close writer
            textWriter.Close();

            Console.ReadLine();
        }
    }
}
