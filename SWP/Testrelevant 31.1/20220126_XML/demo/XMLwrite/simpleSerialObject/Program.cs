﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

namespace simpleSerialObject
{
    [XmlRoot(ElementName = "root")]
    public class SimpleDemo
    {
        [XmlAttribute(AttributeName = "marke")]
        public string Hersteller;
        public string Kennzeichen;
        public int KmStand;
        [XmlIgnore]
        public double Verbrauch;
        
        public string starten()
        {
            return "brmmmmm....";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SimpleDemo d = new SimpleDemo();

            XmlSerializer ser = new XmlSerializer(typeof(SimpleDemo));
            XmlTextWriter tw = new XmlTextWriter("ausgabe.xml", null);

            d.Kennzeichen = "GU 329FU";
            d.KmStand = 116432;
            d.Verbrauch = 6.1;

            ser.Serialize(tw, d);

            tw.Close();
            Console.WriteLine("Objekt serialisiert ...");
            Console.ReadKey();

        }
    }
}
