﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

namespace simpleDeserializeObject
{
    public class SimpleDemo
    {
        public string Kennzeichen;
        public int KmStand;
        public double Verbrauch;
        public string Hersteller;

        public string starten()
        {
            return "brmmmmm....";
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            XmlSerializer ser = new XmlSerializer(typeof(SimpleDemo));
            XmlTextReader rdr = new XmlTextReader("ausgabe.xml");

            SimpleDemo d;
            d = ser.Deserialize(rdr) as SimpleDemo;
            rdr.Close();

            Console.WriteLine("Kennzeichen:" + d.Kennzeichen);
            Console.WriteLine("KmStand: " + d.KmStand);
            Console.WriteLine("Verbrauch: " + d.Verbrauch);
            Console.WriteLine("Hersteller: " + d.Hersteller);

            Console.ReadKey();
        }
    }
}
