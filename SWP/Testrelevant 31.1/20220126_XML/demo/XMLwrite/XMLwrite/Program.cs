﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

namespace XMLwrite
{
    class employee
    {
        private int m_id;
        private string m_firstname;
        private string m_lastname;
        private int m_salary;

        public employee(int id, string firstname, string lastname, int salary)
        {
            m_id = id;
            m_firstname = firstname;
            m_lastname = lastname;
            m_salary = salary;
        }

        public int Id { get { return m_id; } }
        public string Firstname { get { return m_firstname; } }
        public string Lastname { get { return m_lastname; } }
        public int Salary { get { return m_salary; } }
    }

    class Program
    {
        static void Main(string[] args)
        {
            employee[] employees = new employee[4];
            employees[0] = new employee(1, "Peter", "Schmidt", 1000);
            employees[1] = new employee(2, "Sepp", "Müller", 1040);
            employees[2] = new employee(3, "Günter", "Wolfgruber", 1300);
            employees[3] = new employee(4, "Fritz", "Schmied", 1060);

            using (XmlWriter writer = XmlWriter.Create("xmlWriter.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Employees");

                foreach (employee employee in employees)
                {
                    writer.WriteStartElement("Employee");
                    writer.WriteElementString("ID", employee.Id.ToString());
                    writer.WriteElementString("LastName", employee.Lastname.ToString());
                    writer.WriteElementString("FirstName", employee.Firstname.ToString());
                    writer.WriteElementString("Salary", employee.Salary.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();

                Console.WriteLine("Objekt wurde serialisiert ...");
                Console.ReadKey();
            }
        }
    }
}
