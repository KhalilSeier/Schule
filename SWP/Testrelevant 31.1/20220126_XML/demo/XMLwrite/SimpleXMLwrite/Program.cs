﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

namespace SimpleXMLwrite
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();

            doc.LoadXml("<Schüler type='regular'><Name>Tommy ex<Name></Schüler>");
            doc.Save("c:\\simpleXML.xml");

            doc.Save(Console.Out);

            Console.ReadKey();
        }
    }
}
