﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

namespace simpleParsing
{
    class Program
    {
        static void Main(string[] args)
        {
            int ws = 0;
            int pi = 0;
            int dc = 0;
            int cc = 0;
            int et = 0;
            int el = 0;
            int xd = 0;
            int dt = 0;

            XmlTextReader textReader = new XmlTextReader("c:\\books.xml");

            while (textReader.Read())
            {
                XmlNodeType nType = textReader.NodeType;

                switch (nType)
                { 
                    case XmlNodeType.Text:
                        Console.WriteLine("       " + textReader.Value);
                        break;

                    case XmlNodeType.XmlDeclaration:
                        Console.WriteLine("Declaration: " + textReader.Name);
                        dc += 1;
                        break;

                    case XmlNodeType.Comment:
                        Console.WriteLine("Comment: " + textReader.Name);
                        cc += 1;
                        break;

                    case XmlNodeType.Element:
                        int i = textReader.AttributeCount;
                        if (i > 0) Console.WriteLine("Element: " + textReader.Name +
                            " Attribute: " + textReader.GetAttribute(i - 1));
                        else Console.WriteLine("Element: " + textReader.Name);
                        el += 1;
                        break;

                    case XmlNodeType.Entity:
                        Console.WriteLine("Entity: " + textReader.Name);
                        et += 1;
                        break;

                    case XmlNodeType.ProcessingInstruction:
                        Console.WriteLine("Processing instruction: " + textReader.Name);
                        pi += 1;
                        break;

                    case XmlNodeType.DocumentType:
                        Console.WriteLine("Document Type: " + textReader.Name);
                        dt += 1;
                        break;

                    case XmlNodeType.Whitespace:
                        ws += 1;
                        break;
                }
            
            }//while

            Console.WriteLine("---------------------------------");

            Console.WriteLine("Total comments: " + cc.ToString());
            Console.WriteLine("Total elements:" + el.ToString());
            Console.WriteLine("Total entities:" + et.ToString());
            Console.WriteLine("Total processing instructions:" + pi.ToString());
            Console.WriteLine("Total declarations:" + dc.ToString());
            Console.WriteLine("Total document types:" + dt.ToString());
            Console.WriteLine("Total WhiteSpaces:" + ws.ToString());

            Console.ReadKey();
        }
    }
}
