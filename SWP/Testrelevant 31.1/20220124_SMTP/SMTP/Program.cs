﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SMTP
{
    public class clsConnection
    {
        public Socket clientSocket;
        private static int id;
        public string username;
        public enum state { closed, begin, wait, created, recipientsSet, writing, ready};
        public state currentState; 

        public clsConnection(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
            id++;
            currentState = state.begin;
        }

        public clsConnection()
        {
            id++;
            currentState = state.begin;
        }

        public int ID
        {
            get { return id; }
        }
    }

    class Program
    {

        static string ipAddress = "127.0.0.1";
        static IPAddress remoteAddress = IPAddress.Parse(ipAddress);

        static int requestCounts = 0;
        static TcpListener serverSocket = new TcpListener(remoteAddress, 25);
        static Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        static List<clsConnection> connections = new List<clsConnection>();

        static string dataFromClient;
        static string strMessage = String.Empty;

        static void run()
        {
            clsConnection newConnection = new clsConnection(clientSocket);
            connections.Add(newConnection);

            int myId = newConnection.ID;

            Socket mySocket = clientSocket;
            while (true) //until doomsday ...
            {
                Thread.Sleep(50);

                //polling!!!
                if (mySocket.Available != 0) //schaut nach ob Socket-Buffer befüllt ist ...
                {
                    try
                    {
                        requestCounts++; // und schon wieder ein neues Paket ...
                        int bytes = mySocket.Available;
                        byte[] buffer = new byte[bytes];
                        mySocket.Receive(buffer, bytes, SocketFlags.None);
                        dataFromClient = Encoding.ASCII.GetString(buffer);



                        Console.WriteLine("<<  Daten vom Client {0} : length: {1} : {2}", myId, dataFromClient.Length, dataFromClient);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.ToString());
                    }

                    if (dataFromClient.Length > 0)
                    {
                        if (dataFromClient.StartsWith("QUIT"))
                        {
                            newConnection.currentState = clsConnection.state.closed;
                            mySocket.Close();
                            break;//exit while
                        }
                        //message has successfully been received
                        if (dataFromClient.StartsWith("EHLO") && newConnection.currentState == clsConnection.state.begin)
                        {
                            send("250 OK");
                            newConnection.currentState = clsConnection.state.wait;
                            Console.WriteLine("state: {0}", clsConnection.state.wait);
                        }

                        if (dataFromClient.StartsWith("RCPT TO") && newConnection.currentState == clsConnection.state.created)
                        {
                            send("250 OK");
                            newConnection.currentState = clsConnection.state.recipientsSet;
                            Console.WriteLine("state: {0}", clsConnection.state.recipientsSet);
                        }

                        if (dataFromClient.StartsWith("MAIL FROM") && newConnection.currentState == clsConnection.state.wait)
                        {
                            send("250 OK");
                            newConnection.currentState = clsConnection.state.created;
                            Console.WriteLine("state: {0}", clsConnection.state.created);
                        }

                        if (dataFromClient.StartsWith("DATA") && newConnection.currentState == clsConnection.state.recipientsSet)
                        {
                            send("354 Start mail input; end with .");
                            int bytes = mySocket.Available;
                            byte[] buffer = new byte[bytes];
                            mySocket.Receive(buffer, bytes, SocketFlags.None);
                            dataFromClient = Encoding.ASCII.GetString(buffer);
                            Console.Write(">> ");
                            Console.WriteLine(dataFromClient);
                            //send("250 OK");
                            newConnection.currentState = clsConnection.state.writing;
                            Console.WriteLine("state: {0}", clsConnection.state.writing);
                        }

                        if (newConnection.currentState == clsConnection.state.writing)
                        {
                            int bytes = mySocket.Available;
                            byte[] buffer = new byte[bytes];
                            mySocket.Receive(buffer, bytes, SocketFlags.None);
                            dataFromClient = Encoding.ASCII.GetString(buffer);

                            if (!dataFromClient.StartsWith("."))
                            {
                                Console.Write(">> ");
                                Console.WriteLine(dataFromClient);
                            }
                            else
                            {
                                Console.Write(">> ");
                                Console.WriteLine(dataFromClient);
                                newConnection.currentState = clsConnection.state.begin;
                                Console.WriteLine("state: {0}", clsConnection.state.begin);
                            }
                        }
                    }
                }
                else Thread.Sleep(50);
            }
        }

        static public void send(string serverResponse)
        {
            byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse + Environment.NewLine);
            foreach (clsConnection con in connections)
            {
                try
                {
                    con.clientSocket.Send(sendBytes);
                }
                catch
                {
                    //remove lost connection ...
                    connections.Remove(con);
                }
            }
            //clientSocket.Send(sendBytes);
            Console.WriteLine(">> " + serverResponse);
        }

        static void Main(string[] args)
        {
            serverSocket.Start();

            Thread myThread;
            int id = 0;

            while (true)
            {
                id++;
                Console.WriteLine(">> SMTP-Server wurde gestartet ...");
                clientSocket = serverSocket.AcceptSocket(); //blocking ...
                Console.WriteLine(">> wir sind jetzt bereit, Mails entgegenzunehmen");

                //clsConnection newConnection = new clsConnection(clientSocket, id);
                //connections.Add(newConnection);

                myThread = new Thread(run);
                myThread.Start();
            }


            myThread.Join(); //Hauptthread geht schlafen bis der Nebenthread fertig ist. 
            Console.WriteLine(">> Server wurde gestoppt");
            Console.ReadKey();
        }
    }
}
