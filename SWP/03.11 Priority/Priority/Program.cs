﻿using System;
using System.Threading;

namespace Priority
{
    class clsThreading
    {
        public void run1()
        {
            for(int i = 0;i<500;i++)
            {
                Console.Write(".");
            }
        }
        public void run2()
        {
            for (int i = 0; i < 500; i++)
            {
                Console.Write("*");
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            clsThreading obj = new clsThreading();
            Thread thread1, thread2;
            thread1 = new Thread(new ThreadStart(obj.run1));
            thread2 = new Thread(new ThreadStart(obj.run2));

            thread1.Priority = ThreadPriority.Lowest;
            thread2.Priority = ThreadPriority.Highest;


            thread1.Start();
            thread2.Start();

            Console.ReadKey();
        }
    }
}
