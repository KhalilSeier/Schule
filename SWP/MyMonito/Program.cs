﻿using System;

using System.Threading;

namespace MyMonito
{

    class Program
    {
        public static int counter = 0;
        public static Object obj = new Object();
        public static void Increment()
        {
            while(true)
            { 
                Thread.Sleep(100);
                Monitor.Enter(obj);
                counter++;
                Console.WriteLine("Decrement: {0}", counter);
                Monitor.Exit(obj);
            }
        }
        public static void Decrement()
        {
            while(true)
            { 
                Thread.Sleep(100);
                Monitor.Enter(obj);
                counter--;
                Console.WriteLine("Increment: {0}",counter);
                Monitor.Exit(obj);
            }
    }
        static void Main(string[] args)
        {
            Thread thread1, thread2;
            thread1 = new Thread(new ThreadStart(Increment));
            thread2 = new Thread(new ThreadStart(Decrement));

            thread1.Start();
            thread2.Start();

            Console.ReadKey();

        }
    }
}
