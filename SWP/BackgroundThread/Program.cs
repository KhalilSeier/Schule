﻿using System;
using System.Threading;

namespace BackgroundThread
{
    class Program
    {
        public static void run()
        {
            int i = 0;
            while(true)
            {
                i++;
                Console.Write(".");
                Thread.Sleep(2000);
                if(i == 10)
                {
                    break;
                }
            }
        }
        static void Main(string[] args)
        {
            Thread myThread = new Thread(new ThreadStart(run));
            myThread.IsBackground = true;
            myThread.Start();

            int i = 0;
            while (true)
            {
                i++;
                Console.Write("*");
                Thread.Sleep(1000);
                if (i == 5)
                {
                    break;
                }
            }
            Console.ReadKey();
        }
    }
}
