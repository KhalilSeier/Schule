﻿using System;
using System.Numerics;

namespace PrimeChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            
            BigInteger getnumber = 100000000;
            int x = 0;
            while (x < 10)
            {
                getnumber = getnumber * 8;
                Console.WriteLine(getnumber);
                x++;
            }
            BigInteger i = getnumber;

            while (true)
            {
                if(CheckPrim(i))
                {
                    Console.WriteLine("prime: {0}", i);
                }
                i++;
                i = getnumber * 10;
            }
            Console.ReadKey();

        }
        static bool CheckPrim(BigInteger number)
        {
            //if (number <= 1) return false;
            for(BigInteger i = 2; (double)i <Math.Sqrt((double)number);i++)
            {
                if (number % i == 0) return false;
            }
            return true;
        }
    }
}
