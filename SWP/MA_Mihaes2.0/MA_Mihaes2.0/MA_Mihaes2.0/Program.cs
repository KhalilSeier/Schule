﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using MySql.Data.MySqlClient;

namespace MA_Mihaes2._0
{
    public class cls_User
    {
        public UInt32 ID
        {
            get;
            set;
        }

        public string User
        {
            get;
            set;

        }

        public string Password
        {
            get;
            set;

        }

    }
    class WriteIntoDB
    {
        const string CONNECTION_STRING = @"Server=localhost;Database=swp_muster;Uid=root;Pwd=;";
        internal static void ADD_User(cls_User user)
        {
            using (MySqlConnection con = new MySqlConnection(CONNECTION_STRING))
            {
                string sql = "Insert into Kunde(User, Password) values (@user, @passwd";
                MySqlCommand com = new MySqlCommand(sql, con);
                com.Parameters.Add("@user", MySqlDbType.VarChar).Value = user.User;
                com.Parameters.Add("@passwd", MySqlDbType.VarChar).Value = user.Password;
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
        }
    }

    class Server
    {
        private Socket socket;

        public Server(Socket socket1)
        {
            socket = socket1;
        }
        public void Run()
        {
            string[] recieve = new string[4];
            string message = "Welchen Username?";
            string message2 = "Welches Pass?";
            string message3 = "Gib dein User erneut ein";
            string message4 = "Gib dein Pass erneut ein";
            Send(message);
            recieve[0] = GetContent();
            message2 = "Welches Pass?";
            Send(message2);
            recieve[1] = GetContent();
            message3 = "Gib dein User erneut ein";
            Send(message3);
            recieve[2] = GetContent();
            if(recieve[2] == recieve [0])
            {
                message = "Dein User war richtig";
                message4 = "Gib dein Pass erneut ein";
                Send(message4);
                if (recieve[3] == recieve[1])
                {
                    message = "Dein Password war richtig";
                }
                else
                {
                    message = "Password war leider falsch, bitte neu eingeben.";
                    message4 = ("Dein User/Password komb war richtig");
                    Send(message4);
                }
            }
            else
            {
                message = "User war leider falsch, bitte neu eingeben.";
            }
 
            socket.Close();
        }

        private string GetContent()
        {
            string s = "";
            while (true)
            {
                Thread.Sleep(500);
                if (socket.Available > 0)
                {
                    while (socket.Available > 0)
                    {
                        int bytes = socket.Available;
                        byte[] buffer = new byte[bytes];
                        socket.Receive(buffer, bytes, SocketFlags.None);
                        s += Encoding.ASCII.GetString(buffer);
                    }
                    return s;
                }
            }
        }

        public void Send(string serverResponse)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(serverResponse + Environment.NewLine);
            socket.Send(byteArray);
            Console.WriteLine("> " + serverResponse);
        }
        class Program
        {
            static void Main(string[] args)
            {
                string ipAddress = "127.0.0.1";
                IPAddress remoteAddress = IPAddress.Parse(ipAddress);
                TcpListener server = new TcpListener(remoteAddress, 8080);
                server.Start();
                while (true)
                {
                    Socket socket = server.AcceptSocket();
                    Server myServer = new Server(socket);
                    Thread clientThread = new Thread(myServer.Run);
                    clientThread.Start();
                }
            }
        }
    }
}
