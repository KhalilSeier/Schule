﻿using System;
using System.Collections.Generic;

namespace MyDictonary
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, int>();

            dict.Add("cat", 2);
            dict.Add("dog", 3);
            dict.Add("bird", -1);

            Console.WriteLine("(dict1)Anzahl der Einträge: {0}",dict.Count);


            var dict2 = new Dictionary<string, int>()
            {
                {"cat", 2},
                {"dog", 3},
                {"bird", -1},
            };

            Console.WriteLine("(dict2)Anzahl der Einträge: {0}", dict.Count);

            if (dict.ContainsKey("cat"))

            {
                Console.WriteLine("Der Wert von['cat'] = {0}", dict2["cat"]);
            }
            else
            {
                Console.WriteLine("Key value 'cat' is not includet in the dictonary!!!");
            }

            if(dict2.TryGetValue("cat",out int number))
            {
                Console.WriteLine("der Wert von['cat'] = {0}", number);
            }


            foreach(var a in dict2)
            {
                Console.WriteLine(a);
            }

            foreach(KeyValuePair<string,int> pair  in dict2)
            {
                Console.WriteLine("Key: {0} \n Value: {1}",pair.Key,pair.Value);
            }

            Console.ReadKey();

        }
    }
}
